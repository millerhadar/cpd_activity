# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 08:36:58 2016

@author: yaelgr
"""
import powerlaw
import numpy as np

def gen_synth_dist1(xmin,alpha,l): #sythetic powerlaw using the powerlaw package
    synth_dist = powerlaw.Power_Law(xmin=xmin,discrete=True, parameters=[alpha])
    synth_data = synth_dist.generate_random(l)
    return synth_dist,synth_data

def plot_data(fit,title):
    fig = fit.plot_pdf(color = 'r', marker = '*',linestyle = 'None',label = 'data')
    fit.power_law.plot_pdf(color = 'b', linestyle = '--', ax = fig,label = 'power law fit')
    fit.lognormal.plot_pdf(color = 'g',linestyle = ':',ax=fig,label = 'lognormal fit')
    fig.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)#%% 
    fig.set_title(title)   
#%%
'''using the built in generative mechanism of the powerlaw package
generating 2 powerlaw distributions and avaraging them, then testing the comb
distribution for powerlaw'''
#%%
synth_dist1,synth_data1 = gen_synth_dist1(100.0,2.5,100000)
fit_synth1 = powerlaw.Fit(synth_data1,discrete=True)
fit_synth1.power_law.xmin, fit_synth1.power_law.alpha
#%%
r1,p1 =fit_synth1.distribution_compare('power_law', 'lognormal')
#%%
plot_data(fit_synth1,'synthetic data1\nr='+str(round(r1,2))+' p='+str(round(p1,5)))

#%%
synth_dist2,synth_data2 = gen_synth_dist1(100.0,2.5,100000)
fit_synth2 = powerlaw.Fit(synth_data2,discrete=True)
fit_synth2.power_law.xmin, fit_synth2.power_law.alpha
#%%
r2,p2 = fit_synth2.distribution_compare('power_law', 'lognormal')
#%%
plot_data(fit_synth2,'synthetic data2\nr='+str(round(r2,2))+' p='+str(round(p2,5)))

#%%
a = 0.5
comb_data = [round(a*x + (1-a)*y) for x, y in zip(synth_data1, synth_data2)]
fit_comb = powerlaw.Fit(comb_data)
#%%
r_comb,p_comb = fit_comb.distribution_compare('power_law', 'lognormal')
#%%
plot_data(fit_comb,'combined data:\na*synth_data1 + (1-a)*synth_data2 (a=0.5)\nr='+str(round(r_comb,2))+' p='+str(round(p_comb,5)))

#%%
changes_per_node = 1000
number_of_changes = int(0.2*sum(comb_data)/changes_per_node)


#%%
#change in tail
changed_data1 = sorted(comb_data)
for i in range(number_of_changes):
    changed_data1[99999-i] += changes_per_node

fit_comb_changed1 = powerlaw.Fit(changed_data1,discrete=True)
r_comb_changed1,p_comb_changed1 = fit_comb_changed1.distribution_compare('power_law', 'lognormal')

#%%
#%%
#change in head

changed_data2 = sorted(comb_data)
for i in range(number_of_changes):
    changed_data2[i] += changes_per_node

fit_comb_changed2 = powerlaw.Fit(changed_data2,discrete=True)
r_comb_changed2,p_comb_changed2 = fit_comb_changed2.distribution_compare('power_law', 'lognormal')

#%%
#change is random

changed_data3 = comb_data
for i in range(len(comb_data)):
    tmp = i % 10
    if tmp<2:
        changed_data2[i] += changes_per_node

fit_comb_changed3 = powerlaw.Fit(changed_data3,discrete=True)
r_comb_changed3,p_comb_changed2 = fit_comb_changed3.distribution_compare('power_law', 'lognormal')