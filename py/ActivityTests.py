import pandas as pd
import pickle
import networkx as nx
from gini import *
import numpy as np
import igraph
from igraph import *
import re
from dateutil import parser
#import matplotlib as plt
import matplotlib.pyplot as plt
import datetime
import CPD
import pickle
from scipy.stats import spearmanr
import os

class Activity():
    def __init__(self,Activation = None,debug=False):
        self.Debug = debug
        self.cpd = None
        #self.GraphComparison = pd.DataFrame(columns=["Interval", "KS-P-Value"])
        if Activation is not None:
            self.Activation = Activation

    def graph_gini(self,graphs,degree_type , weighted = False):
        l_gini = []
        for next_graph in graphs:
            # 2/3/2019 Replaced by CalcDegreevector with ability to covert networkx to igraph
            #if degree_type == 'in_degree':
            #    tested_degree = sorted([float(d) for n, d in next_graph.in_degree() if d > 0 ], reverse=True)
            #elif degree_type == 'out_degree':
            #    tested_degree = sorted([float(d) for n, d in next_graph.out_degree() if d > 0 ], reverse=True)
            #else:
            #    tested_degree = sorted([float(d) for n, d in next_graph.degree() if d > 0 ], reverse=True)
            #DegreeSample =  np.array(tested_degree)
            DegreeSample = np.array([float(x) for x in self.CalcDegreeVector(next_graph, Weighted=weighted, DegreeType=degree_type)])
            l_gini.append(gini(DegreeSample))
        return np.mean(l_gini),np.std(l_gini),l_gini

    def er_gini(self,n,p,directed, avg = 1 , degree_type = 'all', weighted = False):
        graphs = []
        for i in range(avg):
            graphs.append(nx.erdos_renyi_graph(n, p, directed=directed))
        return self.graph_gini(graphs,degree_type,weighted=weighted)

    def iBarabasi(self,n, m = 0, outpref=True, directed=True, power=2.5, zero_appeal=1, implementation="psumtree_multiple", start_from=None):
        if m == 0:
            g2 = Graph.Barabasi(n=n, outpref=outpref, directed=directed, power=power, zero_appeal=zero_appeal,
                                implementation=implementation,
                                start_from=None)
        else:
            g2 = Graph.Barabasi(n=n, m=m, outpref=False, directed=True, power=2.5, zero_appeal=zero_appeal, implementation="psumtree_multiple",
                            start_from=None)
        return g2

    def convert_igraph_to_networkx(self,graph):
        if graph.is_directed():
            nx_aa = nx.DiGraph()
        else:
            nx_aa = nx.Graph()
        nx_aa.add_nodes_from(range(len(graph.vs)))
        for edge in graph.es:
            nx_aa.add_edge(edge.source, edge.target)
        return nx_aa

    def convert_networkx_to_igraph(self,graph,weighted = True):
        iGraph = Graph(directed=graph.is_directed())
        iGraph.add_vertices(len(graph.nodes))
        edges = {}
        for edge in graph.edges:
            if weighted:
                edges[(edge[0], edge[1])] = edges.get((edge[0], edge[1]), 0) + 1
            else:
                if edge[0] < edge[1]:
                    from_node = edge[0]
                    to_node = edge[1]
                else:
                    from_node = edge[1]
                    to_node = edge[0]
                edges[(from_node, to_node)] = edges.get((from_node, to_node), 0) + 1
        iGraph.add_edges(edges.keys())
        if weighted:
            iGraph.es['weight'] = edges.values()
        return iGraph

    def pa_gini(self,n,m = 0,avg = 1 , degree_type = 'ALL', package = 'NetworkX' , power=2.5 , weighted = False, zero_appeal = 1):
        graphs = []
        for i in range(avg):
            if package=='NetworkX':
                next_graph = nx.barabasi_albert_graph(n, m)
            else:
                next_graph = self.iBarabasi(n=n, m=m, directed=True, power=power, zero_appeal=zero_appeal)
                # next_graph = self.convert_igraph_to_networkx(next_graph)
            graphs.append(next_graph)
        return self.graph_gini(graphs,degree_type, weighted=weighted)

    def Caveman_gini(self,l, k, p, avg = 1, degree_type = 'all'): # l number of clicks , k = click size
        graphs = []
        for i in range(avg):
            graphs.append(nx.relaxed_caveman_graph(l, k, p))
        return self.graph_gini(graphs,degree_type)

    def scale_free_gini(self, n, alpha=0.41, beta=0.54, gamma=0.05, delta_in=0.2, delta_out=0,avg = 1, degree_type = 'all', weighted = False): # l number of clicks , k = click size
        graphs = []
        for i in range(avg):
            graphs.append(nx.scale_free_graph(n=n,alpha=alpha,
                          beta=beta, gamma=gamma, delta_in=delta_in, delta_out=delta_out))
        return self.graph_gini(graphs,degree_type, weighted=weighted)


    def get_graph(self,graphs, name, synthetic=False):
        if synthetic:
            dt = name
        else:
            dt = parser.parse(name[2:5] + " " + name[:2] + " " + name[-4:])
        return graphs[dt]

    def LoadGMLFromFile(self,SourceGML, synthetic=False , package = 'iGraph', full_time = False):
        gmlfiles = os.listdir(SourceGML)
        if synthetic:
            gmlfiles = [x for x in gmlfiles if x[-4:] == '.gml']
        else:
            gmlfiles = [x for x in gmlfiles if x[-4:] == '.gml' and x[:2] == 'g_']
        # gmlfiles = glob.glob(SourceGML + '/g_*.gml')
        graphs = {}
        for f in gmlfiles:
            if package == 'iGraph':
                g = Graph.Read_GML(SourceGML + '/' + f)
            else:
                g = nx.read_gml(SourceGML + '/' + f)
            if synthetic:
                dt = int(f[9:-4])
            else:
                if f[:8] == 'g_Weekly':
                    name = re.sub(r'g_Weekly', '', f)
                elif f[:7] == 'g_ByCPD':
                    name = re.sub(r'g_ByCPD', '', f)
                else:
                    name = re.sub(r'g_WindowSize_30_Delta_30_', '', f)
                name = name[:-4]
                if full_time:
                    dt = parser.parse(name[2:5] + " " + name[:2] + " " + name[5:9]+" "+name[10:12]+":"+name[13:15])
                else:
                    dt = parser.parse(name[2:5] + " " + name[:2] + " " + name[-4:])
            graphs[dt] = g
            # graphs[name] = g
        return graphs

    def get_graph(self,graphs, name, synthetic=False):
        if synthetic:
            dt = name
        else:
            dt = parser.parse(name[2:5] + " " + name[:2] + " " + name[-4:])
        return graphs[dt]

    def CalcDegreeVector(self,gr, DegreeType='ALL', Weighted=True, Loops=True):
        if not isinstance(gr,Graph):
            gr = self.convert_networkx_to_igraph(gr,weighted=Weighted)
        if gr.is_weighted() and Weighted:
            nodes_degree = {}
            if DegreeType == "IN" and gr.is_directed():
                for edge in gr.es:
                    if Loops or edge.target != edge.source:
                        nodes_degree[edge.target] = nodes_degree.get(edge.target, 0) + edge["weight"]
            if DegreeType == "OUT":
                for edge in gr.es:
                    if Loops or edge.target != edge.source:
                        nodes_degree[edge.source] = nodes_degree.get(edge.source, 0) + edge["weight"]
            if DegreeType not in ["IN", "OUT"]:
                for edge in gr.es:
                    if Loops or edge.target != edge.source:
                        nodes_degree[edge.source] = nodes_degree.get(edge.source, 0) + edge["weight"]
                        nodes_degree[edge.target] = nodes_degree.get(edge.target, 0) + edge["weight"]
            deg_vector = nodes_degree.values()
        else:
            if DegreeType == 'ALL':
                deg_vector = gr.degree(loops=Loops)
            elif DegreeType == "IN":
                deg_vector = gr.indegree(loops=Loops)
            else:
                deg_vector = gr.outdegree(loops=Loops)

        return deg_vector

    def calc_generalized_degree(self,g, alpha, degree_type='ALL', Loops=True):
        k = self.CalcDegreeVector(g, DegreeType=degree_type, Weighted=False, Loops=Loops)
        s = self.CalcDegreeVector(g, DegreeType=degree_type, Weighted=True, Loops=Loops)
        return [k_i ** (1 - alpha) * s_i ** alpha for k_i, s_i in zip(k, s)], k, s

    def normalize(self,seq, normalization_size = None):
        if normalization_size:
            m = normalization_size
        else:
            m = max(seq)
        return [x / float(m) for x in seq]

    def generalized_degree_sequence(self,alpha, graphs, Loops=True, synthetic=False,normalized = None):
        gen_deg_sum, gen_indeg_sum, gen_outdeg_sum = [], [], []
        X = []
        counter = 1
        for graph in sorted(graphs.items()):
            X.append(graph[0])
            generalized_degree, degree, weigted_degree = self.calc_generalized_degree(graph[1],
                                                                                 alpha=alpha, degree_type='ALL',
                                                                                 Loops=Loops)
            if normalized:
                gen_deg_sum = self.normalize(gen_deg_sum,normalized[graph[0]])
            # By hadar on 13.6.2019 from sum to avg
            #gen_deg_sum.append(sum(generalized_degree))
            gen_deg_sum.append(sum(generalized_degree)/float(len(generalized_degree)))

            generalized_degree, degree, weigted_degree = self.calc_generalized_degree(graph[1],
                                                                                 alpha=alpha, degree_type='IN', Loops=Loops)
            if normalized:
                gen_indeg_sum = self.normalize(gen_indeg_sum,normalized[graph[0]])
            gen_indeg_sum.append(sum(generalized_degree))
            generalized_degree, degree, weigted_degree = self.calc_generalized_degree(graph[1],
                                                                                 alpha=alpha, degree_type='OUT',
                                                                                 Loops=Loops)
            if normalized:
                gen_outdeg_sum = self.normalize(gen_outdeg_sum,normalized[graph[0]])
            gen_outdeg_sum.append(sum(generalized_degree))


            counter += 1

        return X, gen_deg_sum, gen_indeg_sum, gen_outdeg_sum


    def Calc_Graph_Degree(self,gr, weighted=True):

        nodes_degree, in_degree, out_degree = {}, {}, {}
        for edge in gr.es:
            u = edge.source
            v = edge.target
            if weighted and 'weight' in edge.attributes():
                weight = edge['weight']
            else:
                weight = 1.0
            nodes_degree[u] = nodes_degree.get(u, 0) + weight
            out_degree[u] = out_degree.get(u, 0) + weight
            nodes_degree[v] = nodes_degree.get(v, 0) + weight
            in_degree[v] = in_degree.get(v, 0) + weight

        deg = list(nodes_degree.values())
        in_deg = list(in_degree.values())
        out_deg = list(out_degree.values())
        return deg, in_deg, out_deg

    def Calc_Gini_Seq(self,sfgraphs,Weighted = True,limits = [], counter_limit = None):
        X, y, y2, y3 = [], [], [], []
        counter = 0
        for gr in sorted(sfgraphs):
            if limits == [] or gr.year in limits:
                degree, in_degree, out_degree = self.Calc_Graph_Degree(sfgraphs[gr],weighted=Weighted)
                temp = np.array(out_degree)
                if len(temp) > 0:
                    y.append(gini(temp))
                temp = np.array(in_degree)
                if len(temp) > 0:
                    y2.append(gini(temp))
                temp =np.array(degree)
                if len(temp) > 0:
                    y3.append(gini(temp))
                    X.append(gr)
                counter += 1
                if counter_limit and counter > counter_limit:
                    break

        return X, y ,y2 , y3

    def Calc_Seq_feature(self,sfgraphs,feature,Weighted = True,limits = [], size = 0,counter_limit = None):
        y = []
        counter = 0
        for gr in sorted(sfgraphs):
            if limits == [] or gr.year in limits:
                if feature == 'evcent':
                    # scale = normalize results to 1.
                    feature_list = Graph.evcent(sfgraphs[gr],directed=False,scale=False)
                    y.append(np.mean(feature_list))
                if feature == "transitivity":
                    # Returns the global of the clustering coef
                    tmp = sfgraphs[gr]
                    y.append(Graph.transitivity_undirected(tmp))
                if feature == "closeness":
                    # Returns the mean of the closeness centrality
                    feature_list =  Graph.closeness(sfgraphs[gr])
                    y.append(np.mean(feature_list))
                if feature == "cliques":
                    # Returns the mean of the closeness centrality
                    feature_list =  Graph.cliques(sfgraphs[gr],min=size)
                    y.append(len(feature_list))
                if feature == "NetSizes":
                    y.append([len(sfgraphs[gr].vs),len(sfgraphs[gr].es)])

                counter += 1
                if counter_limit and counter > counter_limit:
                    break

        return y

    def plot_series(self,series = [],yscale=(0,1),grid = True, title= '', ylabel='', xlable ='',colors = None,
                    plot_size = (17,12) , savefile = None , gtx = None , hzones = None , add_scatter = False,
                    markers = ['o'],markersize=12):

        f, ax = plt.subplots(1,figsize=plot_size)
        color_id = 0
        marker_id = 0
        for x,y,label in series:

            if add_scatter:
                if colors:
                    ax.scatter(x, y, label=label, color = colors[color_id], marker=markers[marker_id],
                               s=markersize)
                    ax.plot(x, y, linestyle='--', color = colors[color_id])
                    color_id = (color_id + 1)%len(colors)
                    marker_id = (marker_id + 1) % len(markers)
                else:
                    ax.scatter(x, y, label=label)
                    ax.plot(x, y, linestyle='--')
            else:
                ax.plot(x, y, label=label)
        if gtx:
            gty  = [0] * len(gtx)
            ax.scatter(gtx, gty, s= 10, marker='^',label='ground truth')
            for x in gtx:
                ax.axvline(x=x, color='green', linestyle='--', linewidth=1)
        if yscale != []:
            ax.set_ylim(ymin=yscale[0],ymax=yscale[1])
        if hzones:
            for zone in hzones:
                plt.axhspan(zone[0], zone[1], facecolor=zone[2], alpha=zone[3])
        plt.legend()
        plt.grid(grid)
        plt.title(title)
        plt.ylabel(ylabel)
        plt.xlabel(xlable)
        plt.show()
        if savefile:
            f.savefig(savefile, dpi=300)

    def get_group_color(self,x,groups = [] , default_color = 'green'):
        found = False
        for group,color in groups:
            if x in group:
                ret = color
                found = True
        if not found:
            ret = default_color
        return ret

    def plot_graph(self,graph,groups = [], weighted = False , layout ="fruchterman_reingold" , size = (500,500), margin=20 ,
                   vertex_size = 30, tofile = None, weight_mult = 0.1):
        visual_style = {}
        visual_style["vertex_size"] = vertex_size
        visual_style["vertex_color"] = [self.get_group_color(x,groups) for x in graph.vs["label"]]
        #visual_style["vertex_shape"] = "triangle-up"
        visual_style["vertex_label"] = graph.vs["name"]
        if weighted:
            visual_style["edge_width"] = [weight_mult*x for x in graph.es["weight"]]
        else:
            visual_style["edge_width"] = [1 for x in graph.es["weight"]]
        #"kamada_kawai" , "kk" , "circle"
        layout = graph.layout(layout,weights=visual_style["edge_width"])
        visual_style["layout"] = layout
        visual_style["bbox"] = size
        visual_style["margin"] = margin
        prt = plot(graph, **visual_style)
        if tofile:
            prt.save(tofile, dpi=300)

        return prt

    def draw_feature_time(self,df_gini_psi, show_data_ids, metrics,plot_delta,direction, hor_line,
                          GraphComperisonFile = None,cpd_p_value = 0.9,plot_cpd_points=0,percent=False,draw_type='scatter',bins = 1,
                          cdf=True, histtype = 'step',cumulative = True,pin_size = 20, lgd ='upper right',SetID = -1,
                          ltrim =1,rtrim = 0,print_stats = False,to_file = None,debug=False,max_y = 0, min_y = 0):
        self.stats = {}
        colors = ['blue',   'orange', 'brown', 'magenta','olive','cyan',
                  'black', 'gold', 'lightcoral', 'teal', 'salmon', 'wheat', 'lavender', 'darkgreen', 'darkgray',
                  'darkred',
                  'skyblue']
        data_rows = [df_gini_psi[df_gini_psi['SetID'] == x].index[0] for x in show_data_ids]
        f, ax = plt.subplots(1, figsize=(10, 8))
        color_index = 0
        min_y,max_y = min_y,max_y
        # ltrim = 1 # Trim the first ltrim points from the dataset (when the starting date is not valid)
        for metric_y in metrics:
            for data_row in data_rows:
                col = colors[color_index%len(colors)]
                #print(data_row,metric_y[0])
                #xm = df_gini_psi[metric_y[0]].iloc[data_row]
                xm = df_gini_psi[metric_y[0]][data_row]

                if plot_delta:
                    #print(direction)
                    xm = np.array(self.cal_delta(xm, direction,percent=percent))
                #edates = self.str_to_dates(str(df_gini_psi['X4'].iloc[data_row]))
                edates = self.str_to_dates(str(df_gini_psi['X4'][data_row]))
                xm = self.net_clean(xm, data_row)
                edates = self.net_clean(edates,data_row)
                xm = xm[ltrim:]
                edates = edates[ltrim:]
                if rtrim > 0:
                    xm = xm[:-1*rtrim]
                    edates = edates[:-1*rtrim]
                if draw_type == 'scatter':
                    if min(xm) < min_y:
                        min_y = min(xm)
                    if max(xm) > max_y:
                        max_y = max(xm)
                    color_index += 1
                    plt.plot(edates, xm, color=col)
                    plt.xlabel("Time")
                    if GraphComperisonFile:
                        gcf = '../FramewokExec/'+GraphComperisonFile+'/Interval7_7/GraphComparison_Interval.csv'
                        GrapgComperison = pd.read_csv(gcf, sep=',')
                        GrapgComperison['Interval'] = pd.to_datetime(GrapgComperison['Interval'], format="%Y-%m-%d")
                        GrapgComperison = GrapgComperison.replace(np.nan, 0, regex=True)
                        intervals = GrapgComperison['Interval'] .tolist()
                        p_values = interval = GrapgComperison['KS-P-Value'] .tolist()
                        Vertices = interval = GrapgComperison['Vertices'].tolist()

                        intervals = intervals[ltrim:]
                        p_values = p_values[ltrim:]
                        Vertices = Vertices[ltrim:]

                        maxV = max(Vertices)
                        VerticesNorm = [tmp/float(maxV) for tmp in Vertices]
                        pin_sizev = [max(1,int(pin_size * tmp**10)) for tmp in VerticesNorm]
                        #print(pin_sizev)
                        NumOfInteractions = GrapgComperison['NumOfInteractions'].tolist()
                        p_values = [1-tmp for tmp in p_values]
                        Events = GrapgComperison[GrapgComperison['KS-P-Value'] < 1- cpd_p_value]
                        events_date = Events['Interval'].tolist()
                        events_p_pal = Events['KS-P-Value'].tolist()

                        events_date  = events_date[ltrim:]
                        events_p_pal = events_p_pal[ltrim:]

                        events_p_pal = [1-tmp for tmp in events_p_pal]
                        for event in events_date:
                            ax.axvline(x=event, color='green', linestyle='--', linewidth=1)
                        if plot_cpd_points == 1:
                            ax.scatter(events_date, events_p_pal, color='green', marker='*', s=pin_size )
                        if plot_cpd_points == 2:
                            ax.scatter(intervals, p_values, color='green',marker= '*',s=pin_size )
                    else:
                        pin_sizev = [pin_size] * len(xm)

                    ax.scatter(edates, xm, s=pin_sizev, color=col,
                               marker='o', label=metric_y[1])

                    if hor_line is not None:
                        plt.plot(edates, [hor_line] * len(edates))
                if draw_type == 'hist':
                    if debug:
                        print(xm)
                    tmp = []
                    for w in range(len(xm)):
                        if np.isnan(xm[w]):
                            tmp.append(0)
                        else:
                            tmp.append(xm[w])
                    xm = tmp
                    self.stats[df_gini_psi['data'].iloc[data_row]] = [np.mean(xm),np.std(xm)]
                    if print_stats:
                        print(df_gini_psi['data'].iloc[data_row],np.mean(xm),np.std(xm))
                    ax.hist(xm,bins=bins, label=df_gini_psi['data'].iloc[data_row]
                    , density = cdf, histtype = histtype,cumulative = cumulative)
        if draw_type == 'scatter':
            ax.set_ylim(min_y , max(max_y + 0.1,1))
        if plot_delta:
            suf = " Delta "
        else:
            suf = ''
        if percent:
            suf += " %"
        #yint = range(min(xm), math.ceil(max(xm)) + 1)
        #plt.pyplot.yticks(yint)
        #ax.title.set_text(metric_y[1]+suf)
        #plt.ylabel(pref + metric_y[1])
        if lgd != 'None':
            plt.legend(loc=lgd)
        if to_file is not None:
            f.savefig(to_file + '.png')
        plt.show()



    #****************************************************
    # Run all inequality tests
    #****************************************************
    def cal_delta(self,seq, direction=False, percent = False,changes = True):
        act_delta = seq
        if changes:
            act_delta = []
            for i in range(len(seq) - 1):
                if direction is False:
                    if percent:
                        if max(seq[i],seq[i+1]) == 0:
                            act_delta.append(0)
                        else:
                            act_delta.append((seq[i] - seq[i + 1]) / float(max(seq[i],seq[i+1])))
                    else:
                        act_delta.append(seq[i] - seq[i + 1])
                else:
                    if percent:
                        if max(seq[i],seq[i+1]) == 0:
                            act_delta.append(0)
                        else:
                            act_delta.append(abs(seq[i] - seq[i + 1]) / float(max(seq[i],seq[i+1])))

                    else:
                        act_delta.append(abs(seq[i] - seq[i + 1]))
            act_delta.append(0)
        return act_delta

    def str_to_dates(self,param):
        ret = []
        my_dates = param[1:-1].split('),')
        for cur_date in my_dates:
            ymd = cur_date.lstrip()[18:].strip().split(',')
            ret.append(datetime.datetime(year=int(ymd[0]),
                                         month=int(ymd[1]),
                                         day=int(ymd[2])))
        return ret

    def inequality_test(self,DataName,DataDir,ver = "ver3" ,DIR = '../FramewokExec/',sub_dir = 'ActiveMap',
                        graph_normalization=None,StorResults = True,nick = '',full_time=False, SetID = -1,
                        years_limits=[],drop_prev=True, debug=False):
        if not self.cpd:
            self.cpd = CPD.CPD()
        # *********************
        # Load graphs from directory
        # *********************
        if sub_dir == '':
            sub_dir = 'ActiveMap'
        if DataName == "Synthetic":
            synthetic = True
        else:
            synthetic = False
        use_file = DIR + DataName +"/"+sub_dir+"/"+DataDir +"/"
        sfgraphs = self.LoadGMLFromFile(use_file, synthetic = synthetic,full_time = full_time)
        if debug:
            print("graphs path : ", use_file)
            print("number of graphs: ",len(sfgraphs))
        # *********************
        # Calculate Activity for each graph
        # *********************
        alpha0_0 = 0.0
        alpha0_25 = 0.25
        alpha0_5 = 0.5
        alpha0_75 = 0.75
        alpha1_0 = 1.0
        norm = 0

        if DataName == "Synthetic":
            with open(DIR + DataName + "/" +sub_dir + '/GroundTruth.pickle', 'rb') as fp:
                GT = pickle.load(fp)
            events = GT['Interval'].tolist()
            if debug:
                print("Events Loaded")

        if graph_normalization:
            with open(use_file + 'DaysToNormalize.pickle', 'rb') as handle:
                graph_normalization = pickle.load(handle)
            # change the normalization of enrorn ( Ubuntu ) to weeks instead of days
            for x in graph_normalization:
                graph_normalization[x] = int(graph_normalization[x] / 7)
            if debug:
                print("graph normalization is set")


        X0, phi0_0, phi_in_0_0, phi_out_0_0 = self.generalized_degree_sequence(alpha0_0, sfgraphs,
                                                                             synthetic=synthetic,
                                                                             normalized=graph_normalization)
        X1, phi0_25, phi_in_0_25, phi_out_0_25 = self.generalized_degree_sequence(alpha0_25, sfgraphs,
                                                                                synthetic=synthetic,
                                                                                normalized=graph_normalization)
        X2, phi0_5, phi_in_0_5, phi_out_0_5 = self.generalized_degree_sequence(alpha0_5, sfgraphs,
                                                                             synthetic=synthetic,
                                                                             normalized=graph_normalization)
        X3, phi0_75, phi_in_0_75, phi_out_0_75 = self.generalized_degree_sequence(alpha0_75, sfgraphs,
                                                                                synthetic=synthetic,
                                                                                normalized=graph_normalization)
        X4, phi1_0, phi_in_1_0, phi_out_1_0 = self.generalized_degree_sequence(alpha1_0, sfgraphs,
                                                                             synthetic=synthetic,
                                                                             normalized=graph_normalization)
        zeta = [min(h, h / float(p + 0.00001)) for h, p in zip(phi1_0, phi0_0)]
        zeta_in = [min(h, h / float(p + 0.00001)) for h, p in zip(phi_in_1_0, phi_in_0_0)]

        if debug:
            print("Zeta : ", zeta)
        # *********************
        # Calculate Gini Coeficients
        # *********************
            # y = out degree , y2 = in degree , y3= degree
        wX, wy, wy2, wy3 = self.Calc_Gini_Seq(sfgraphs=sfgraphs, Weighted=True, limits=years_limits)
        uwX, uwy, uwy2, uwy3 = self.Calc_Gini_Seq(sfgraphs=sfgraphs, Weighted=False, limits=years_limits)

        if debug:
            print("Gini's:", wy3)


        # *********************************
        # Calculate EigenValues Centrality (Means for each graph)
        # ********************************
        eigeny = 0
        # eigeny = self.Calc_Seq_feature(sfgraphs=sfgraphs,feature='evcent')

        # *********************************
        # Calculate Clustering Coef Centrality (Means of transitivity)
        # ********************************
        ClusteringCoef = self.Calc_Seq_feature(sfgraphs=sfgraphs, feature='transitivity')
        if debug:
            print("Clustering Coefs:", ClusteringCoef)

        # *********************************
        # Calculate Closeness Centrality (Means of transitivity)
        # ********************************
        Closeness = self.Calc_Seq_feature(sfgraphs=sfgraphs, feature='closeness')
        if debug:
            print("Closeness's:", Closeness)

        # *********************************
        # Calculate Cliques  (Number of Cliques) of minimal size = 4
        # ********************************
        Clique4 = 0
        #Clique4 = self.Calc_Seq_feature(sfgraphs=sfgraphs, feature='cliques', size=4)

        # *********************************
        # Calculate Cliques  (Number of Cliques) of minimal size = 5
        # ********************************
        Clique5 = 0
        #Clique5 = self.Calc_Seq_feature(sfgraphs=sfgraphs, feature='cliques', size=5)

        # *********************************
        # Calculate Cliques  (Number of Cliques) of minimal size = 7
        # ********************************
        Clique7 = 0
        #Clique7 = self.Calc_Seq_feature(sfgraphs=sfgraphs, feature='cliques', size=7)

        # *********************************
        # Calculate Network Sizes
        # ********************************
        netsizes = self.Calc_Seq_feature(sfgraphs=sfgraphs, feature='NetSizes')
        NodesSize = [x[0] for x in netsizes]
        EdgesSize = [x[1] for x in netsizes]

        # *************************************
        # Store all results in pickle and excell
        # ***************************************
        output_name = DIR + 'MultiDataset/ActiveMap/' + ver + '/Gini_Psi_results'
        if os.path.isfile(output_name +".pickle"):
            with open(output_name+".pickle", 'rb') as fp:
                df_gini_psi = pickle.load(fp)
        else:
            df_gini_psi = pd.DataFrame(columns=["SetID","data", "subdir","zeta", "zeta_norm", "wX", "uwX",
                     "wy3","uwy3","wy2","uwy2","X1","X4",'phi0_0','phi1_0','phi_in_0_0', 'phi_in_1_0',
                    'EigenValueMean','ClusteringCoef'])
        df_gini_psi = df_gini_psi.astype('object')
        if drop_prev and len(df_gini_psi[df_gini_psi['data'] == DataName + " " + nick]) > 0:
            df_gini_psi = df_gini_psi.drop(df_gini_psi[df_gini_psi['data'] == DataName + " " + nick].index)
        df_gini_psi = df_gini_psi.append({
            "SetID": SetID,
            "data": DataName + " " + nick,
            "subdir": sub_dir,
            "zeta": zeta,
            'zeta_noram': 0,
            'wX': wX,
            'uwX': uwX,
            'wy3': wy3,
            'uwy2': uwy2,
            'wy2': wy2,
            'uwy3': uwy3,
            'X1': X1,
            'X4': X4,
            'phi0_0': phi0_0,
            'phi1_0': phi1_0,
            'phi_in_0_0': phi_in_0_0,
            'phi_in_1_0': phi_in_1_0,
            'EigenValueMean':eigeny,
            'ClusteringCoef' : ClusteringCoef,
            'Closeness': Closeness,
            'Clique4' : Clique4,
            'Clique5': Clique5,
            'Clique7': Clique7,
            'NodesSize':NodesSize,
            'EdgesSize':EdgesSize,
            'proccessed': datetime.datetime.now()
        }, ignore_index=True)
        df_gini_psi = df_gini_psi.astype('object')
        if StorResults:
            df_gini_psi.to_excel(output_name +'.xlsx')
            df_gini_psi.to_pickle(output_name +'.pickle')
        return df_gini_psi

    def net_feature(self,df_gini_psi, net_id, feature, changes, direction, percent):
        ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == net_id][feature].tolist(), direction=direction,
                               percent=percent, changes=changes)
        dname = df_gini_psi[df_gini_psi['SetID'] == net_id]['data'].iloc[0]

        if net_id == 11:  # Ubuntu
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 11][feature].tolist()[0][105:], direction=direction,
                               percent=percent, changes=changes)
        if net_id == 10:  # WikiC
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 10][feature].tolist()[0][200:-10],
                               direction=direction, percent=percent, changes=changes)
        if net_id == 9:  # WikiT
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 9][feature].tolist()[0][250:], direction=direction,
                               percent=percent, changes=changes)
        if net_id == 7:  # mailEU
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 7][feature].tolist()[0][1:-2], direction=direction,
                               percent=percent, changes=changes)
        if net_id == 8:  # faceB
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 8][feature].tolist()[0][100:], direction=direction,
                               percent=percent, changes=changes)
        if net_id == 4:  # manuM
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 4][feature].tolist()[0][:], direction=direction,
                               percent=percent, changes=changes)
        if net_id == 17:  # EnronFull
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 17][feature].tolist()[0][50:-5], direction=direction,
                               percent=percent, changes=changes)
        if net_id == 3:  # EnronWeekly
            ret = self.cal_delta(df_gini_psi[df_gini_psi['SetID'] == 3][feature].tolist()[0][40:-15], direction=direction,
                               percent=percent, changes=changes)
        return dname,ret

    def net_clean(self,feature,net_id):
        ret = feature
        if net_id == 11:  # Ubuntu
            ret = feature[105:]
        if net_id == 10:  # WikiC
            ret = feature[200:-10]
        if net_id == 9:  # WikiT
            ret = feature[250:]
        if net_id == 7:  # mailEU
            ret = feature[1:-2]
        if net_id == 8:  # faceB
            ret = feature[100:]
        if net_id == 4:  # manuM
            ret = feature[:]
        if net_id == 17:  # EnronFull
            ret = feature[50:-5]
        if net_id == 3:  # EnronWeeklyManagers
            ret = feature[45:-15]
        return ret