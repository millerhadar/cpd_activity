import igraph
from igraph import *
import matplotlib.dates as mdates

import matplotlib.pyplot as plt
import numpy as np
import math
import random
from scipy.stats import ks_2samp
import re
from collections import defaultdict
#import glob
import os
import pickle
import dateutil
import datetime
from plfit import plfit
from plpva import plpva
import powerlaw
from dateutil.parser import parse
from datetime import timedelta
import csv
import pandas as pd
import os
import logging
import time


class CPD():
    def __init__(self, WindowSize = 'None',debug=False):

        self.graphs = {}
        if WindowSize in ['Daily', 'Weekly', 'Monthly', 'Bi-Monthly']:
            self.Window_Size = WindowSize
        self.FitedTable = pd.DataFrame(columns=["Interval", "alpha", "xmin", "L", "vertices", "edges",
                                "p-value","gof","KS-P-Value","KS-D","prev-p-value","prev-gof",
                                'PLVsExponentialR','PLVsExponentialP-val',
                                'PLVsLognormalR', 'PLVsLognormalP-val',
                                'PLVsTruncated_power_lawR', 'PLVsTruncated_power_lawP-val','Vertices'])
        self.DegreeVector = pd.DataFrame(columns=["Interval", "DegreeVector", 'Model', 'Params','CP'])
        self.Window_Size = WindowSize
        self.IgnoredGraphInterval = []
        timestr = time.strftime("%Y%m%d-%H%M")
        logging.basicConfig(filename='Logs/SlidingWindowTest_'+timestr+'.log', level=logging.INFO)
        self.Debug = debug

    def SetObjectParameters(self,winsize = None , Debug = None ):
        if not winsize is None:
            self.Window_Size = winsize
        if not Debug is None:
            self.Debug = Debug

    def CalcNextWindowFrame(self,StartDate, increment = True):
        if self.Window_Size == "Monthly":
            if not increment:
                StartDate += timedelta(days=-30)
            following = StartDate + timedelta(days=32)
            following2 = following + timedelta(days=32)
            NewStart = datetime.datetime(following.year, following.month, 1)
            NewStop = datetime.datetime(following2.year, following2.month, 1)
        else:
            if self.Window_Size == "Bi-Monthly":
                if not increment:
                    StartDate += timedelta(days=-30)
                if StartDate.day < 15:
                    NewStart = datetime.datetime(StartDate.year, StartDate.month, 16)
                    following = StartDate + timedelta(days=32)
                    NewStop = datetime.datetime(following.year, following.month, 1)
                else:
                    following = StartDate + timedelta(days=32)
                    NewStart = datetime.datetime(following.year, following.month, 1)
                    NewStop = datetime.datetime(following.year, following.month, 16)
            else:
                if self.Window_Size == 'Weekly':
                    intervaldelta = timedelta(days=7)
                if self.Window_Size == 'Daily':
                    intervaldelta = timedelta(days=1)
                if self.Window_Size == 'Interval':
                    intervaldelta = timedelta(days=self.SlidingWindow[0])
                if not increment:
                    PrevMonday = StartDate.weekday() * -1
                    if PrevMonday < 0:
                        NewStart = StartDate + datetime.timedelta(days=PrevMonday )
                    else:
                        NewStart = StartDate
                else:
                    if self.Window_Size =="Interval":
                        NewStart = StartDate + timedelta(days=self.SlidingWindow[1])
                    else:
                        NewStart = StartDate + intervaldelta
                NewStop = NewStart + intervaldelta
        #print(NewStart,NewStop)
        return NewStart, NewStop

    def GenerateGraphSequence2(self, LinksFile, WindowSize= None, SlidingWindow= [30,1] ,Weighted=True, Directed=False,
                               DegreeType='ALL',OutputDir=None, pref=None, Limit=0):
        ''' This Procedure generates a GML files and Triads files from the links pickle'''
        self.DegreeVector = self.DegreeVector[0:0]
        self.create_directory(OutputDir)
        if LinksFile[-3:].lower() == 'csv':
            with open(LinksFile, 'rb') as f:
                reader = csv.reader(f)
                links = list(reader)
        else:
            with open(LinksFile, 'rb') as fp:
                links = pickle.load(fp)
        # Window Size
        if WindowSize is None:
            if self.Window_Size is None:
                if self.Debug:
                    logging.info("Window size is not set")
                return -1
        else:
            if WindowSize in ['Daily','Weekly','Monthly', 'Bi-Monthly','Interval']:
                self.Window_Size = WindowSize
                self.SlidingWindow = SlidingWindow
            else:
                if self.Debug:
                    logging.info("Wrong Parameter for Window Size Please refere to the manual")
                return -1
        if pref is None:
            if self.Window_Size == 'Interval':
                prefix_file_name = "WindowSize_" + str(self.SlidingWindow[0]) + "_Delta_" + str(self.SlidingWindow[1])+"_"  # Days,Weeks
            else:
                prefix_file_name = self.Window_Size  # Days,Weeks
        else:
            prefix_file_name = pref

        counter = 0

        IntervalStart , IntervalStop = self.CalcNextWindowFrame(links.timestamp.iloc[0],increment = False)
        while IntervalStop < links.timestamp.iloc[len(links)-1] and (Limit == 0 or counter < Limit):
            edges = {}
            nodes = {}
            WinLinks = links.loc[(links['timestamp'] >= IntervalStart) &
                                        (links['timestamp'] < IntervalStop)]
            for idx in range(len(WinLinks)):
                s = links.source.iloc[idx]
                r = links.target.iloc[idx]
                nodes[s] = 1
                nodes[r] = 1
                if Directed:
                    edges[(s, r)] = edges.get((s, r), 0) + 1
                else:
                    if s < r:
                        edges[(s, r)] = edges.get((s, r), 0) + 1
                    else:
                        edges[(r, s)] = edges.get((r, s), 0) + 1
            g = igraph.Graph(directed=Directed)
            g.add_vertices([str(node) for node in nodes])
            if Weighted:
                for edge in edges:
                    g.add_edge(str(edge[0]), str(edge[1]), weight=edges[edge])
            else:
                for edge in edges:
                    g.add_edge(str(edge[0]), str(edge[1]))
            g.vs["label"] = g.vs["name"]
            IntervalStartSTR = IntervalStart.strftime("%d%b%Y")
            interval = 'g_' + prefix_file_name + IntervalStartSTR + '.gml'
            self.graphs[interval] = g
            if OutputDir is not None:
                filename = 'g_' + prefix_file_name + IntervalStartSTR + '.gml'
                filename = OutputDir + '/' + filename
                g.save(filename, format='gml')



                degree_vector = self.CalcDegreeVector(g, DegreeType, Weighted)
                if len(degree_vector) < 1:
                    self.IgnoredGraphInterval.append(interval)
                    if self.Debug:
                        logging.info("Warning: The degree vector is null at: " + str(interval) + " Graph Ingoned!")
                else:
                    GraphDate = dateutil.parser.parse(interval[-13:-11] + '/' + interval[-11:-8]
                                                      + '/' + interval[-8:-4])
                    self.DegreeVector = self.DegreeVector.append(
                        {"Interval": GraphDate.strftime("%Y-%m-%d"), "DegreeVector": degree_vector}, ignore_index=True)
                self.DegreeVector['Interval'] = self.DegreeVector['Interval'].astype('datetime64[ns]')


            if self.Debug:
                logging.info("Writing network file: ", filename, len(edges), "links")
            IntervalStart, IntervalStop = self.CalcNextWindowFrame(IntervalStart)
            counter += 1
        self.DegreeVector.to_csv(OutputDir + '/DegreeVector_' + self.Window_Size + '.csv', encoding='utf-8',
                                 index=False)
        self.DegreeVector.to_pickle(OutputDir + '/DegreeVector_' + self.Window_Size + '.pickle')
        return self.graphs

    ''' This function is an OLD version ( Obsolete )'''
    def GenerateGraphSequence(self, LinksFile, WindowSize= None, SlidingWindow= [30,1] ,Weighted=True, Directed=False,
                              OutputDir=None, pref=None, Format='None', Limit=10):
        ''' This Procedure generates a GML files and Triads files from the links pickle'''
        if LinksFile[-3:].lower() == 'csv':
            with open(LinksFile, 'rb') as f:
                reader = csv.reader(f)
                links = list(reader)
        else:
            with open(LinksFile, 'rb') as fp:
                links = pickle.load(fp)
        edges = {}
        nodes = {}
        # Window Size
        if WindowSize is None:
            if self.Window_Size is None:
                if self.Debug:
                    print ("Window size is not set")
                return -1
        else:
            if WindowSize in ['Daily','Weekly','Monthly', 'Bi-Monthly','Interval']:
                self.Window_Size = WindowSize
                self.SlidingWindow = SlidingWindow
            else:
                if self.Debug:
                    print("Wrong Parameter for Window Size Please refere to the manual")
                return -1
        if pref is None:
            if self.Window_Size == 'Interval':
                prefix_file_name = "WindowSize_" + str(self.SlidingWindow[0]) + "_Delta_" + str(self.SlidingWindow[1])+"_"  # Days,Weeks
            else:
                prefix_file_name = self.Window_Size  # Days,Weeks
        else:
            prefix_file_name = pref

        IntervalStart , IntervalStop = self.CalcNextWindowFrame(links[0][2],increment = False)
        #startdate = links[0][2]


        counter = 0
        # email_ID_dict=dict((k,v) for v,k in enumerate(addresses))
        for link in links:
            # if link occurs after current week, write to file and start new week
            if not link[2] < IntervalStop:
                IntervalStartSTR = IntervalStart.strftime("%d%b%Y")
                if self.Debug:
                    print ("Writing network file: ", filename, len(edges), "links")
                if Format == 'ALL' or Format == 'TRIAD':
                    triad_f = 't_' + prefix_file_name + IntervalStartSTR + '.triad'
                    triad_f = OutputDir + '/' + triad_f
                    triad_out = open(triad_f, "w")
                    for edge in edges:
                        triad_out.write("%i\t%i\t%i\n" % (edge[0], edge[1], edges[edge]))
                    triad_out.close()
                g = igraph.Graph(directed=Directed)
                g.add_vertices([str(node) for node in nodes])
                if Weighted:
                    for edge in edges:
                        g.add_edge(str(edge[0]), str(edge[1]), weight=edges[edge])
                else:
                    # g.add_edges([edge for edge in edges])
                    for edge in edges:
                        g.add_edge(str(edge[0]), str(edge[1]))
                g.vs["label"] = g.vs["name"]
                self.graphs['g_' + prefix_file_name + IntervalStartSTR + '.gml'] = g
                if Format == 'ALL' or Format == 'GML':
                    filename = 'g_' + prefix_file_name + IntervalStartSTR + '.gml'
                    filename = OutputDir + '/' + filename
                    g.save(filename,format='gml')
                edges = {}
                nodes = {}
                #print(IntervalStart,IntervalStop)
                IntervalStart , IntervalStop = self.CalcNextWindowFrame(IntervalStart)
                counter += 1
                if Limit > 0 and counter > Limit:
                    break
            s = link[0]
            r = link[1]
            nodes[s] = 1
            nodes[r] = 1
            if Directed:
                edges[(s, r)] = edges.get((s, r), 0) + 1
            else:
                if s < r:
                    edges[(s, r)] = edges.get((s, r), 0) + 1
                else:
                    edges[(r, s)] = edges.get((r, s), 0) + 1
        return self.graphs

    def LoadGMLFromFile(self,SourceGML):
        gmlfiles = os.listdir(SourceGML)
        gmlfiles = [x for x in gmlfiles if x[-4:] == '.gml' and x[:2] == 'g_']
        # gmlfiles = glob.glob(SourceGML + '/g_*.gml')
        self.graphs = {}
        for f in gmlfiles:
            g = Graph.Read_GML(SourceGML + '/' + f)
            name = re.sub(r'.*Enron' + self.Window_Size + 'GML/', '', f)
            self.graphs[name] = g
        return self.graphs

    def gml_to_vector(self,g, structure='degree'):
        vect = np.zeros(200)
        if structure == 'degree':
            struct = g.degree(mode=ALL, loops=False)
        if structure == 'eigenvector':
            struct = g.eigenvector_centrality(directed=False, weights='weight')
        if structure == 'betweeness':
            struct = g.betweenness(directed=False)
        vs = VertexSeq(g)
        counter = 0
        for v in vs:
            vect[int(v['id'])] = struct[counter]
            counter += 1
        return (vect)

    def StoreFitedPlaw(self,Destination):
        with open(Destination +'/Plow_Fit_'+self.Window_Size+'.pickle', 'wb') as fp:
            pickle.dump(self.PlowFit, fp)

    def RestoreFitedPlaw(self,Location):
        with open (Location + '/g_Fit_'+self.Window_Size+'.pickle', 'rb') as fp:
            PlowFit = pickle.load(fp)

    def LoadFitDataFromFile(self,filename, sep=','):
        self.FitedTable = pd.read_csv(filename, sep=',')
        return self.FitedTable

    def LoadDegreeVectorFromFile(self,filename):
        self.DegreeVector = pd.read_pickle(filename)
        return self.DegreeVector

    def CalcDegreeVector(self,gr,DegreeType = 'ALL', Weighted = True):

        if gr.is_weighted() and Weighted:
            nodes_degree = {}
            if DegreeType == "IN" and gr.is_directed():
                for edge in gr.es:
                    nodes_degree[edge.target] = nodes_degree.get(edge.target, 0) + edge["weight"]

            if DegreeType == "OUT":
                for edge in gr.es:
                    nodes_degree[edge.source] = nodes_degree.get(edge.source, 0) + edge["weight"]
            if DegreeType not in ["IN", "OUT"]:
                for edge in gr.es:
                    nodes_degree[edge.source] = nodes_degree.get(edge.source, 0) + edge["weight"]
                    nodes_degree[edge.target] = nodes_degree.get(edge.target, 0) + edge["weight"]
            deg_vector = [nodes_degree[node] for node in nodes_degree]
        else:
            if DegreeType == 'ALL':
                deg_vector = gr.degree()
            elif DegreeType == "IN":
                deg_vector = gr.indegree()
            else:
                deg_vector = gr.outdegree()

        return deg_vector

    def Fit(self, DegreeVector,PDFType="Discrete", version="JMM",Finite = False):
        if version == "JMM":
            if PDFType == "Cont":
                DegreeVector = [float(i) for i in DegreeVector]
            if self.Debug:
                print(DegreeVector)
            if Finite:
                alpha, xmin, L = plfit(DegreeVector,'finite')
            else:
                alpha, xmin, L = plfit(DegreeVector)
        else:
            L = 0
            p = 0
            gof = 0
        return (alpha, xmin, L)


    def FitAll2(self, DegreeType='ALL', Mode = 'Graph',Weighted=True, PDFType="Discrete",Finite = False, MinNodesToFit=10, OutputDir=None, Limit=0,
               py_ver="JMM"):
        from datetime import datetime

        counter = 1
        self.FitedTable = self.FitedTable[0:0]
        self.DegreeVector = self.DegreeVector[0:0]
        if Mode == 'Graph':
            if len(self.graphs) == 0:
                if self.Debug:
                    logging.info("The Graph Sequence is not set up")
                return -1
            for interval in self.graphs:
                degree_vector = self.CalcDegreeVector(self.graphs[interval], DegreeType, Weighted)
                if len(degree_vector) < 1:
                    self.IgnoredGraphInterval.append(interval)
                    if self.Debug:
                        logging.info("Warning: The degree vector is null at: " + str(interval)+ " Graph Ingoned!")
                else:
                    GraphDate = dateutil.parser.parse(interval[-13:-11] + '/' + interval[-11:-8]
                                                      + '/' + interval[-8:-4])
                    self.DegreeVector = self.DegreeVector.append(
                        {"Interval": GraphDate.strftime("%Y-%m-%d"), "DegreeVector": degree_vector}, ignore_index=True)
            self.DegreeVector['Interval'] = self.DegreeVector['Interval'].astype('datetime64[ns]')
            self.DegreeVector.sort_values('Interval', ascending=True, inplace=True)
            if not OutputDir is None:
                self.DegreeVector.to_csv(OutputDir + '/DegreeVector_' + self.Window_Size + '.csv', encoding='utf-8',
                                         index=False)
                self.DegreeVector.to_pickle( OutputDir + '/DegreeVector_' + self.Window_Size + '.pickle')
        else:
            self.LoadDegreeVectorFromFile(OutputDir + '/DegreeVector_Interval.pickle')
        for  idx in range(len(self.DegreeVector)):
            degree_vector = self.DegreeVector.iloc[idx]['DegreeVector']
            if self.Debug:
                logging.info("Fitting " + str(idx) + " out of " + str(len(self.DegreeVector)))
                logging.info(degree_vector)
            if len(degree_vector) < MinNodesToFit:
                alpha = 0
                xmin = 0
                L = 0
            else:
                alpha, xmin, L = self.Fit(degree_vector, DegreeType, py_ver,Finite = Finite)
            self.FitedTable = self.FitedTable.append({"Interval": self.DegreeVector.iloc[idx]['Interval'],
                                                      "alpha": alpha,
                                                      "xmin": xmin,
                                                      "L": L,
                                                      "vertices": len(degree_vector),
                                                      "edges": 0,
                                                      "min_degree": min(degree_vector),
                                                      "max_degree": max(degree_vector),
                                                      'Vertices':len(degree_vector)}, ignore_index=True)
            counter += 1
            if Limit > 0 and counter > Limit:
                break
        if Mode == 'Graph':
            self.FitedTable['Interval'] = self.FitedTable['Interval'].astype('datetime64[ns]')
        self.FitedTable.sort_values('Interval', ascending=True, inplace=True)
        if not OutputDir is None:
            self.FitedTable.to_csv(OutputDir + '/GraphComparison_' + self.Window_Size + '.csv', encoding='utf-8',
                                   index=False)
        return self.FitedTable

    def PlawPval(self, DegreeVector, xmin , PDFType="Discrete", reps=1000):
        p, gof = plpva(DegreeVector, xmin, 'reps', reps, 'silent')

        return (p,gof)

    def CalcConfidence(self,Reps = 1000,ConfidenceType = ['Current'],OutputDir = None,Limit = 0):
        from datetime import datetime

        counter = 1
        if len(self.FitedTable)<1 :
            logging.info(len(self.FitedTable))
            if self.Debug:
                logging.info("The Fit table was not initiated")
            return -1
        if len(self.DegreeVector) < 1:
            if self.Debug:
                logging.info("The Degree Vector was not initiated")
            return -1

        for idx in range(len(self.FitedTable)):
            if np.isnan(self.FitedTable.iloc[idx]['p-value']):
                if self.Debug:
                    logging.info("Interval: " + str(idx) + " out of: " + str(len(self.FitedTable)))
                interval = self.FitedTable.iloc[idx]['Interval']
                deg = self.DegreeVector.loc[self.DegreeVector['Interval'] == interval]
                if deg.iloc[0]["DegreeVector"] is list:
                    deg = deg.iloc[0]["DegreeVector"]
                else:
                    deg = deg.iloc[0]["DegreeVector"][1:-1]
                    if deg[-3:] == '...':
                        deg = deg[:-5]
                        deg = [float(x) for x in deg.split(',')]
                cur_xmin = self.FitedTable.iloc[idx]['xmin']
                if 'Current' in ConfidenceType:
                    if cur_xmin > 0:
                        p, gof = self.PlawPval(deg, int(cur_xmin), reps=Reps)
                    else:
                        p = -1
                        gof = -1
                    self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('p-value')] = p
                    self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('gof')] = gof

                if "Compare" in ConfidenceType  and idx > 1:
                    p = self.FitedTable.iloc[idx]['p-value']
                    if p > 0.1:
                        fit = powerlaw.Fit(deg, verbose=False, descrete=False)
                        exp_R, exp_p = fit.distribution_compare('power_law', 'exponential')
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('PLVsExponentialR')] = exp_R
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('PLVsExponentialP-val')] = exp_p

                        tpl_R, tpl_p = fit.distribution_compare('power_law', 'truncated_power_law')
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('PLVsTruncated_power_lawR')] = tpl_R
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('PLVsTruncated_power_lawP-val')] = tpl_p

                        lnr_R, lnr_p = fit.distribution_compare('power_law', 'lognormal')
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('PLVsLognormalR')] = lnr_R
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('PLVsLognormalP-val')] = lnr_p

                if  'Prev' in ConfidenceType  and idx > 1:
                    prev_xmin = self.FitedTable.iloc[idx - 1]['xmin']
                    prev_p = self.FitedTable.iloc[idx - 1]['p-value']
                    p = self.FitedTable.iloc[idx]['p-value']
                    max_deg = self.FitedTable.iloc[idx - 1]['max_degree']
                    if prev_xmin > 0 and prev_p > 0.1 and p > 0.1 and prev_xmin < max_deg:
                        p, gof = self.PlawPval(deg, prev_xmin, reps=Reps)

                    else:
                        p = -1
                        gof = -1
                    self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('prev-p-value')] = p
                    self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc('prev-gof')] = gof

                if  'KS' in ConfidenceType :
                    if idx > 0 :
                        prev_interval = self.FitedTable.iloc[idx-1]['Interval']
                        prev_deg = self.DegreeVector.loc[self.DegreeVector['Interval'] == prev_interval]
                        if type(prev_deg.iloc[0]["DegreeVector"]) == str:
                            prev_deg = prev_deg.iloc[0]["DegreeVector"][1:-1]
                            if prev_deg[-3:] == '...':
                                prev_deg = prev_deg[:-5]
                            prev_deg = [float(x) for x in prev_deg.split(',')]
                            logging.info('here')
                        else:
                            prev_deg = prev_deg.iloc[0]["DegreeVector"]
                        ks_D, ks_p = ks_2samp(deg, prev_deg)
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc("KS-P-Value")] = ks_p
                        self.FitedTable.iloc[idx, self.FitedTable.columns.get_loc("KS-D")] = ks_D
                if OutputDir is not None and idx % 10 == 0:
                    logging.info("Confidence of Interval: " + str(idx) + " out of: " + str(len(self.FitedTable)))
                    self.FitedTable.to_csv(OutputDir + '/GraphComparison_' + self.Window_Size + '.csv', encoding='utf-8',
                                           index=False)
            else:
                if self.Debug:
                    logging.info("Interval: " + str(idx) + "Skipped")
            counter += 1
            if Limit > 0 and  counter > Limit:
                break
        if not OutputDir is None:
            self.FitedTable.to_csv(OutputDir + '/GraphComparison_' + self.Window_Size + '.csv', encoding='utf-8',
                                   index=False)
            self.DegreeVector.to_csv(OutputDir + '/DegreeVector_' + self.Window_Size + '.csv', encoding='utf-8',
                                     index=False)
        return self.FitedTable

    def PlotCPD(self,FoundCPD,Events,Params,reference = []):
        title = ""
        fig, ( ax2) = plt.subplots(nrows = 1, ncols = 1,dpi=80,figsize=(15, 4))
        days = []
        impressions = []
        for ev in sorted(FoundCPD):
            days.append(ev)
            #impressions.append(4- FoundCPD[ev])
            impressions.append(FoundCPD[ev])
        ax2.scatter(days, impressions,c = 'blue', label = "CPD")
        days = []
        impressions = []
        for ev in sorted(Events):
            days.append(ev)
            impressions.append(1.2)
        ax2.scatter(days, impressions,c = 'red', label = "GT")
        if 'Latto' in reference:
            ldays = []
            limpressions = []
            LattoCPD = self.LoadPeel('../output2/PeelClausetCPD.csv')
            for cpd in sorted(LattoCPD):
                ldays.append(cpd)
                limpressions.append(1.4)
            ax2.scatter(ldays, limpressions, c='green', label="Latto")
        #[Mode, Threshold, PLThreshold, MinSampleNodes,filters]
        if Params[0] == "PL":
            title = "Power Low p-value with thershold of "+ str(Params[2])+ " , p-val distance >" + str(Params[1])
        if Params[0] == "KS":
            title = "KS two sample test,  p-value > "+ str(Params[1])
        if len(Params[4]) > 0:
            title = title + " Filters: "
            for filter in Params[4]:
                title = title +     filter[0] + ":" + str(filter[1])
        plt.title("Change Points using " + title)
        plt.ylabel("Level")
        plt.grid(True)

        #plt.plot_date(x=days, y=impressions)
        plt.legend(loc='best')
        plt.ylim((0,3))
        plt.show()

    def plot_output3d(self,ktitle, triplets_dic, xlabel, ylabel, zlabel,to_file = None):
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        import operator
        font = {'family': 'serif',
                'color': 'darkred',
                'weight': 'normal',
                'size': 16,
                }

        fig = plt.figure(dpi=80, figsize=(13, 12))
        # fig = plt.subplots(nrows=6, ncols=1, sharex=True, dpi=80, figsize=(13, 12))
        ax = fig.add_subplot(111, projection='3d')
        xs = []
        ys = []
        zs = []
        ranked = []
        max_triple = (0, 0, 0)
        max_rank = 0

        for hyperparam in triplets_dic:
            xs.append(hyperparam[0])
            ys.append(hyperparam[1])
            zs.append(hyperparam[2])
            # zs.append(triplets_dic[hyperparam][0])
            # ranked.append(triplets_dic[hyperparam][0])
            ranked.append(triplets_dic[hyperparam])
            if triplets_dic[hyperparam] > max_rank:
                max_triple = hyperparam
                max_rank = triplets_dic[hyperparam]
        colmap = cm.ScalarMappable(cmap='coolwarm')
        colmap.set_array(ranked)
        # yg = ax.scatter(xs, ys, zs, c=cm.hsv(ranked/max(ranked)), marker='o')
        yg = ax.scatter(xs, ys, zs, c=cm.hsv(ranked), marker='o')
        cb = fig.colorbar(colmap)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)
        plt.title(ktitle, fontdict=font)
        if to_file is not None:
            fig.savefig(to_file + '.png')
        plt.show()
        print("Best HyperParameters", str(max_triple), str(triplets_dic[hyperparam]))

    def PlotFeatures(self,Features,Ev,threshold = 0.1):
        fig, axs = plt.subplots(nrows=6, ncols=1, sharex=True, dpi=80, figsize=(13, 12))
        # fig, ax1 = plt.subplots()

        x = Features['Interval']
        y1 = Features['max_degree']
        y2 = Features['vertices']
        y3 = Features['KS-D']
        y4 = Features['KS-P-Value']
        y5 = Features['p-val']
        y6 = Features['PLVsExponentialP-val']
        y61 = Features['PLVsLognormalP-val']
        y62 = Features['PLVsTruncated_power_lawP-val']

        gt = []
        int_gt = []
        for d in sorted(Features['Interval']):
            if d in Ev:
                gt.append(0)
                int_gt.append(d)

        axs[0].plot(x, y1, 'r-')
        axs[0].set(xlabel='Date', ylabel='Max Degree')
        axs[0].plot(int_gt, gt, 'g*')

        axs[1].plot(x, y2, 'r-')
        axs[1].set(xlabel='Date', ylabel='# of Vertices')
        axs[1].plot(int_gt, gt, 'g*')

        axs[2].plot(x, y3, 'b-')
        axs[2].plot(int_gt, gt, 'g*')
        axs[2].set(xlabel='Date', ylabel='KS D val')

        axs[3].plot(x, y4, 'b-', label='KS p-val')
        axs[3].set(xlabel='Date', ylabel='KS p-val')
        axs[3].plot(int_gt, gt, 'g*', label='Event')
        horiz_line_data = np.array([1-threshold for i in xrange(len(x))])
        lower_horiz_line_data = np.array([threshold for i in xrange(len(x))])
        axs[3].plot(x, horiz_line_data, 'r--', label='Upper Threshold')
        axs[3].plot(x, lower_horiz_line_data, 'r--', label='Lower Threshold')
        axs[3].legend(loc="upper right")

        axs[4].plot(x, y5, 'b-', label='Power Low p-val')
        axs[4].set(xlabel='Date', ylabel='PL p-val')
        axs[4].plot(int_gt, gt, 'g*', label='Event')
        horiz_line_data = np.array([threshold for i in xrange(len(x))])
        axs[4].plot(x, horiz_line_data, 'r--', label='Lower Threshold')
        axs[4].legend(loc="upper right")

        axs[5].plot(x, y6, 'g-', label='exp')
        axs[5].plot(x, y61, 'r-', label='lognr')
        axs[5].plot(x, y62, 'y-', label='trnPL')
        axs[5].plot(x, y5, 'b-', label='PL')
        axs[5].set(xlabel='Date', ylabel='Fit Compare')
        axs[5].legend(loc="upper right")

        plt.show()

    def PlotCM(self,X, Features, title,verticals=True, to_file = None):
        fig = plt.figure(dpi=80, figsize=(13, 4))
        # fig = plt.plots(nrows=1, ncols=1, sharex=True, dpi=80, figsize=(13, 12))
        color = ['r', 'b', 'g', 'y', 'b','k']
        counter = 100
        for i in range(len(Features)):
            ax = plt.subplot(111)
            ax.plot(X, Features[i][0], color[i] + '-', label=Features[i][1])
            max_x = X[Features[i][0].index(max(Features[i][0]))]
            if verticals:
                plt.axvline(x=max_x, color=color[i], linestyle='--', label=str(max_x)+':'+str(round(max(Features[i][0]),2)))
            counter += 1
        #for line in range(len(verticals)):
        #    plt.axvline(x=verticals[line],color=color[line],linestyle='--', label=str(verticals[line]))
        plt.title(title)
        plt.legend(loc='best')
        if to_file is not None:
            fig.savefig(to_file + '.png')
        plt.show()

    def LoadFeatures(self,filename, sep=','):
        self.FitedTable = pd.read_csv(filename, sep=',')
        self.FitedTable = self.FitedTable.rename(columns={'p-value': 'p-val'})
        #self.FitedTable['Interval'] = self.FitedTable['Interval'].astype('datetime64[ns]')
        self.FitedTable['Interval'] = pd.to_datetime(self.FitedTable['Interval'], format="%d/%m/%Y")
        return self.FitedTable

    def LoadEvents(self,filename, filters = [('Latto',1)],sep=','):
        Events = pd.read_csv(filename, sep=',')
        #Events['Date'] = Events['Date'].astype('datetime64[ns]')
        Events['Date'] = pd.to_datetime(Events['Date'], format="%d/%m/%Y")
        self.EventsVector = self.GetEventsVector(Events, Filters=filters)
        return self.EventsVector

    def LoadPeel(self,filename,sep=','):
        PeelCPDVector = {}
        PeelCPD = pd.read_csv(filename, sep=',')
        PeelCPD['Date'] = pd.to_datetime(PeelCPD['Date'], format="%d/%m/%Y")
        for i in range(1, len(PeelCPD)):
            PeelCPDVector[PeelCPD.iloc[i]['Date']] = 1

        #PeelCPDVector = self.GetEventsVector(Events, Filters=filters)
        return PeelCPDVector

    '''
    This function compute the events vectors related to the window size.
    If the window size is WEEK, then this function collapse all the events 
    within that week to a single point in time (monday). Days Fridat-Sunday
    are computed to the next Monday. Days Tuesday-Thursday are computed to the previos Monday.
    The Original event list could be filtered acording to the parameters.
    Foe example sending [('Latto',1)] as filter will return only the events that used by
    Latto & Clauset 2015
    '''
    def GetEventsVector(self, allEvents, Filters = [], WinSize = 'Weeks' ):
        EventsVector = {}
        for name,val in Filters:
            allEvents =  allEvents.loc[allEvents[name] == val]
        if WinSize == 'Weeks':
            counter = 1
            for OrigDate in allEvents['Date']:
                if OrigDate.weekday() < 4:
                    eventdate = OrigDate + datetime.timedelta(days=-OrigDate.weekday())
                else:
                    eventdate = OrigDate + datetime.timedelta(days=7-OrigDate.weekday())
                EventsVector[eventdate] = EventsVector.get(eventdate,0) + 1
                counter += 1
                #if counter > 20:
                #    break
        return EventsVector


    '''
    If the edges of the data (beggining or ending) contain information bellow the minimum 
    threshold then it is trimed.
    Pleas note that this function trim the margins only. so if the datum in the middle 
    of the data goes bellow the minimum it is not trimed
    '''
    def TrimLowSample(self,ddata,elist, MinNodes = 10 , MinEdges=10):
        DataRowsToDelete = []
        for idx in range(len(ddata)):
            if ddata.iloc[idx]['vertices'] < MinNodes or ddata.iloc[idx]['edges'] < MinEdges:
                DataRowsToDelete.append(idx)
                if elist.has_key(ddata.iloc[idx]['Interval']):
                    elist.pop(ddata.iloc[idx]['Interval'])
            else:
                break
        DataRowsToDelete.sort()
        ddata =  ddata.drop(ddata.index[DataRowsToDelete])
        return ddata,elist


    def GetCPDVector(self,DistData, Mode = 'PL',Threshold=0.8,PLThreshold = 0.1,steps=2, startrow=2):
        CPDVector = {}

        DistData.sort_values('Interval', ascending=True, inplace=True)
        if startrow < 2:
            startrow = 2
        if Mode == 'PL':
            for i in range(startrow, len(DistData)):
                # Check if this week just exited Power Law Confidence (p-value Bellow 0.1)
                if DistData.iloc[i]['p-val'] <= PLThreshold and DistData.iloc[i - 1]['p-val'] > PLThreshold:
                    CPDVector[DistData.iloc[i]['Interval']] = 1

                # Check that in a window frame of 'steps' itervals the confidence level change
                # exeeds the threshold
                else:
                    if i > steps:
                        MaxPval = DistData[i - steps:i - 1]['p-val'].max()
                        MinPval = DistData[i - steps:i - 1]['p-val'].min()
                        Distance = abs(DistData.iloc[i]['p-val'] - MinPval)
                        if Distance > Threshold:
                            CPDVector[DistData.iloc[i]['Interval']] = Distance
                        Distance = abs(DistData.iloc[i]['p-val'] - MaxPval)
                        if Distance > Threshold:
                            CPDVector[DistData.iloc[i]['Interval']] = Distance
        if Mode == 'KS':
            for i in range(startrow, len(DistData)):
                # Check if this week just exited Power Law Confidence (p-value Bellow 0.1)
                if DistData.iloc[i]['KS-P-Value'] >= Threshold:
                    CPDVector[DistData.iloc[i]['Interval']] = DistData.iloc[i]['KS-P-Value']
        return CPDVector

    def ConfusionMatrix(self,GroundTruth, Results, SampleSize, Tolerance=0):
        cm = {}
        cm['tp'] = []
        cm['tn'] = []
        cm['fp'] = []
        cm['fn'] = []
        for event in GroundTruth:
            Nx_event = event + datetime.timedelta(days=7)
            Pv_event = event + datetime.timedelta(days=-7)
            if event in Results or (Nx_event in Results and Tolerance == 1) or (Pv_event in Results and Tolerance == 1):
                cm['tp'].append(event)
            else:
                cm['fn'].append(event)

        # cm['tn'] = SampleSize - cm['tp'] - cm['fp'] - cm['fn']
        # cm['Accuracy'] = (cm['tp'] + cm['tn']) / float(SampleSize)
        # cm['Presision'] = float(cm['tp']) / (cm['tp'] + cm['fp'])
        cm['Recall'] = float(len(cm['tp'])) / (len(cm['tp']) + len(cm['fn']))
        return cm

    def ChangePointDetection(self,FeaturesFile,EventsFile,EventsFilter =[],reference = ['Latto'],Mode = 'PL',MinSampleNodes = 0 , MinSampleEdges = 0 , Threshold=0.8, PLThreshold = 0.1,
                             steps=2,Outputs = ['Plot','ConfusionMatrix'] ):
        self.LoadFeatures(FeaturesFile)
        self.LoadEvents(EventsFile,EventsFilter)
        if MinSampleNodes > 0 or MinSampleEdges > 0:
            self.FitedTable, self.EventsVector = self.TrimLowSample(self.FitedTable, self.EventsVector, MinNodes = MinSampleNodes , MinEdges=MinSampleEdges)

        FoundCPD = self.GetCPDVector(self.FitedTable, Mode=Mode,Threshold=Threshold, PLThreshold=PLThreshold,steps=steps)
        CPDResults = self.ConfusionMatrix(self.EventsVector, FoundCPD, len(self.FitedTable), Tolerance=1)
        if 'Plot' in Outputs:
            self.PlotCPD(FoundCPD, self.EventsVector, [Mode,Threshold,PLThreshold,MinSampleNodes,EventsFilter],reference = reference)
        if 'ConfusionMatrix' in Outputs:
            print("True Positive: %s"%len(CPDResults['tp']))
            print("False Negative: %s"%len(CPDResults['fn']))
            print("Recall: %s"%CPDResults['Recall'])

        return CPDResults,FoundCPD

    def CPDbyActivation(self,Activation):
        if Activation['Master']['Generate']:
            g3 = self.GenerateGraphSequence2(LinksFile=Activation['Directory'] + 'BitCoinLinks.pickle',
                                             WindowSize=Activation['HyperParams']['WindowMode'],
                                             SlidingWindow=Activation['HyperParams']['Slider'],
                                             Weighted=True, Directed=False,
                                             OutputDir=Activation['Directory'] + 'Interval' + str(
                                                 Activation['HyperParams']['Slider'][0]) + '_' +
                                                       str(Activation['HyperParams']['Slider'][1]), Limit=0)
        else:
            g3 = self.LoadGMLFromFile(
                Activation['Directory'] + 'Interval' + str(Activation['HyperParams']['Slider'][0]) + '_' +
                str(Activation['HyperParams']['Slider'][1]))
        if Activation['Master']['Fit']:
            # SynG.SetObjectParameters(Debug = True)
            fit1 = self.FitAll2(Weighted=True, DegreeType='ALL', PDFType="Cont", MinNodesToFit=5, Finite=True,
                                py_ver="JMM", OutputDir=Activation['Directory'] + 'Interval' + str(
                    Activation['HyperParams']['Slider'][0]) + '_' +
                                                        str(Activation['HyperParams']['Slider'][1]), Limit=0)
            # SynG.SetObjectParameters(Debug = False)
        if Activation['Master']['Conf']:
            Directory = Activation['Directory'] + 'Interval' + str(Activation['HyperParams']['Slider'][0]) + '_' + str(
                Activation['HyperParams']['Slider'][1])
            self.SetObjectParameters(Debug=True)
            self.LoadFitDataFromFile(Directory + "/GraphComparison_" + Activation['HyperParams']['WindowMode'] + ".csv")
            self.LoadDegreeVectorFromFile(
                Directory + "/DegreeVector_" + Activation['HyperParams']['WindowMode'] + ".pickle")
            conf = self.CalcConfidence(Reps=100, ConfidenceType=Activation['Master']['ConfType'], OutputDir=Directory,
                                       Limit=0)
            self.SetObjectParameters(Debug=False)
        if 'plot_params' in Activation:
            KSCPDResults, KSchangePoints = self.ChangePointDetection(
                FeaturesFile=Activation['Directory'] + '/Interval' + str(Activation['HyperParams']['Slider'][0]) + '_' +
                             str(Activation['HyperParams']['Slider'][1]) + '/GraphComparison_Interval.csv',
                EventsFile=Activation['Directory'] + '/BitCoinEventsFullCSV.csv',
                # EventsFilter =[('EventType','Regulation')],
                EventsFilter=Activation['EventsFilter'],
                dates=Activation['TestingDates'],
                reference=Activation['plot_params']['plot_reference'],
                Mode=Activation['Master']["ConfType"][0],
                MinSampleNodes=0,
                Threshold=Activation['CmModes'][0][1],
                Outputs=['Plot', 'ConfusionMatrix'],
                plot_params=Activation['plot_params'])

    def GetVerticesDegree(self,Distribution, Size):
        if Distribution == 1:  # PowerLaw
            Xmin = np.random.choice(np.arange(1, 10))
            scale = 2 + np.random.beta(1, 1)
            model = powerlaw.Power_Law(xmin=Xmin, parameters=[scale])
            synthetic_data = model.generate_random(Size)
            PL = [math.ceil(i) for i in synthetic_data]
            return (["PowerLaw", [scale, Xmin], PL,0])

        if Distribution == 2:  # Gamma
            scale = 2
            shape = 1
            Gamma = np.random.gamma(shape, scale, Size)
            Gamma = [math.ceil(i) for i in Gamma]
            return (["Gamma", [scale, shape], Gamma,1])

        if Distribution == 3:  # Exponential
            scale = 2.5
            Exponential = np.random.exponential(scale, Size)
            Exponential = [math.ceil(i) for i in Exponential]
            return (["Exponential", [scale], Exponential,1])

        if Distribution == 4:  # Poisson
            p_lambda = 2
            Poisson = []
            Poisson1 = np.random.poisson(p_lambda, Size)
            for i in Poisson1:
                if i>0:
                    Poisson.append(i)
                else:
                    Poisson.append((i+1))
            Poisson = [math.ceil(i) for i in Poisson]
            return (["Poisson", [p_lambda], Poisson,1])

        if Distribution == 5:  # betta as  Uniforam
            alpha = 1
            beta = 1
            Betta = np.random.beta(alpha, beta, Size)
            Betta = [math.ceil(i*100) for i in Betta]
            return (["Betta", [alpha, beta], Betta,1])

    def CreateSyntheticData(self,P_Change_Point,Size = 50,Method = 'Random',BTParam = [],outdir = None,
                            MinSampleSize = 20 , MaxSampleSize = 100, CreateGML = False):
        self.DegreeVector = pd.DataFrame(columns=["Interval", "DegreeVector", 'Model', 'Params','CP'])
        if Method == 'Bootstrap':
            if len(BTParam) < 2:
                return -1
            PLg = Graph.Read_GML(BTParam[0])
            NonPLg = Graph.Read_GML(BTParam[1])
            power_law_degree = self.CalcDegreeVector(PLg)
            non_power_law_degree = self.CalcDegreeVector(NonPLg)
        for index in range(1, Size):

            if Method =='Bootstrap':
                Distribution = np.random.choice(np.arange(1, 3), p=[P_Change_Point, (1 - P_Change_Point)])
                if Distribution == 1:
                    world = power_law_degree
                    g1 = ["PowerLaw","Bootstrap " + BTParam[0],[],0]
                else:
                    world = non_power_law_degree
                    g1 = ["Non-PowerLaw", "Bootstrap " + BTParam[1], [],1]
                NumberOfVertices = random.randint(MinSampleSize, len(world))
                g1[2] = random.sample(world, NumberOfVertices)
            if Method =='GenerativeModel':
                Power = 3
                fw_prob = 0.5
                NumberOfVertices = random.randint(MinSampleSize, MaxSampleSize)
                CPInterval = np.random.choice(np.arange(1, 3), p=[P_Change_Point, (1 - P_Change_Point)])
                if CPInterval == 2:
                    theoretical_dist = powerlaw.Power_Law(xmin=1.0, parameters=[Power])
                    sim_data = theoretical_dist.generate_random(NumberOfVertices)
                    sim_data = [int(s) for s in sim_data]
                    NextIntervalGraxph = Graph.Barabasi(NumberOfVertices, sim_data, power=Power, zero_appeal=20,
                                                       directed=True, implementation='psumtree_multiple')
                    g1 = ['Preferential Attachment',[NumberOfVertices],NextIntervalGraph.degree(),0]
                else:
                    #NextIntervalGraph = Graph.Forest_Fire(NumberOfVertices, fw_prob, bw_factor=0.0, ambs=1,
                    #                                      directed=True)
                    #g1 = ['Forest Fire', [NumberOfVertices, fw_prob], NextIntervalGraph.degree(), 1]
                    NextIntervalGraph = Graph.Watts_Strogatz(dim=1,size=NumberOfVertices,
                                                          nei=1, p=fw_prob,loops=True,multiple=True)
                    watts_degree = [x for x in NextIntervalGraph.degree() if x > 0]
                    g1 = ['Watts_Strogatz', [NumberOfVertices, fw_prob], watts_degree, 1]
                if CreateGML:
                    NextIntervalGraph.save(outdir + '/g_WindowSize_n_Delta_n_Interval' + str(index),
                                           format='gml')
            if Method == 'Random':
                Distribution = np.random.choice(np.arange(1, 6),
                                                p=[P_Change_Point, (1 - P_Change_Point) / 4, (1 - P_Change_Point) / 4,
                                                   (1 - P_Change_Point) / 4, (1 - P_Change_Point) / 4])
                NumberOfVertices =random.randint(MinSampleSize, MaxSampleSize)
                g1 = self.GetVerticesDegree(Distribution, NumberOfVertices)

            self.DegreeVector = self.DegreeVector.append({"Interval": index,
                                                "DegreeVector": g1[2],
                                                "Model": g1[0],
                                                "Params": g1[1],
                                                "CP": g1[3]}, ignore_index=True)
        if outdir != None:
            self.DegreeVector.to_csv(outdir + '/DegreeVector_Interval.csv', encoding='utf-8', index=False, mode='a')
            self.DegreeVector.to_pickle(outdir + '/DegreeVector_Interval.pickle')
        return self.DegreeVector

    def GetNonPL(self,Vertices, exp=3):
        model = 'Exponential'
        # sim_dataXP = []
        sim_dataXP = [math.ceil(x) for x in np.random.exponential(1.0 / exp, Vertices)]
        return model, exp, sim_dataXP

    def GetPowerLawSample(self,n,alpha= 2.5,Xmin = 1,mode='Discrete',Xmax = None):
        # Inverse Transform Sampling
        model = 'PowerLaw'
        expt = 1.0/(1.0-alpha)
        ysamples = np.random.random(n)
        sample = Xmin*ysamples**expt
        if Xmax is not None:
            sample=sample[sample<Xmax]
        if mode == 'Discrete':
            sample = [float(int(x)) for x in sample]
        return model,alpha,sample

    def GetExponentialSample(self,n, my_lambda = 2.5 ,mode='Discrete',Xmax = None):
        model = 'Exponential'
        ysamples = np.random.random(n)
        sample = np.log(1-ysamples)/(-1 * my_lambda)
        if mode == 'Discrete':
            sample = [np.ceil(1/x) for x in sample]
        else:
            sample = [1/x for x in sample]
        if Xmax is not None:
            sample=[x for x in sample if x < Xmax]
        return model,my_lambda,sample

    def GetLogNormalSample(self,n,mu=0,sigma = 1,Xmin = 0 , Xmax = None,mode='Discrete'):
        model = 'LogNormal'
        s = np.random.normal(mu, sigma, size=n)
        s = [np.exp(mu+sigma*x) for x in s]
        s = [x for x in s if x>=Xmin]
        if Xmax is not None:
            s=[x for x in s if x<=Xmax]
        if mode == 'Discrete':
            s = [np.ceil(x) for x in s]
        return model,mu,s

    def GetPL(self,Vertices, rate=3,type ='np'):
        model = 'PowerLaw'
        # sim_dataPL = []
        if type == 'powerlaw':
            theoretical_dist = powerlaw.Power_Law(xmin=1.0,parameters=[rate],discrete = True)
            sim_dataPL = [int(t) for t in theoretical_dist.generate_random(Vertices)]
        else:
            sim_dataPL = [float(int(1 / x)) for x in np.random.power(rate, Vertices)]
        return model, rate, sim_dataPL

    def GetNorm(self,MyMean, MySTD):
        # retrun a random integer with mean myMean and std=mySTD
        rndVal = MySTD * np.random.normal(size=1) + MyMean
        return int(round(rndVal))

    def GenerateRawData(self,NumberOfVerrices, LifeTime, EventRate, ContunityMEAN, ContunitySTD, Dir):
        DegreeVector = pd.DataFrame(columns=["Interval", "DegreeVector", 'Model', 'Params', 'CP'])
        intervalID = 1
        counter = 1
        self.create_directory(Dir)
        NextIntervalType = 0
        while counter < LifeTime:
            if EventRate == 0:
                NextIntervalType = (NextIntervalType + 1) % 2
            else:
                NextIntervalType = np.random.choice(np.arange(1, 3), p=[(1 - EventRate), EventRate])
            NextTypeLength = self.GetNorm(ContunityMEAN, ContunitySTD)
            NextNumOfVertices = NumberOfVerrices
            if NextTypeLength <1:
                NextTypeLength = 1
            if NextTypeLength > (LifeTime - counter):
                NextTypeLength = LifeTime - counter
            for IntervalTypeIndex in range(NextTypeLength):
                if not isinstance(NumberOfVerrices, int):
                    NextNumOfVertices = self.GetNorm(NumberOfVerrices[0], NumberOfVerrices[1])
                if NextNumOfVertices < 50:
                    NextNumOfVertices = 50
                if NextIntervalType == 1:
                    #model, params, degrees = self.GetPL(NextNumOfVertices)
                    model, params, degrees = self.GetPowerLawSample(NextNumOfVertices,alpha= 2.5,Xmin = 1,mode='Discrete')
                    CP = intervalID
                else:
                    #model, params, degrees = self.GetNonPL(NextNumOfVertices)
                    model, params, degrees = self.GetExponentialSample(NumberOfVerrices,my_lambda = 2.5,mode='Discrete')
                    CP = intervalID * -1
                DegreeVector = DegreeVector.append({"Interval": counter, "DegreeVector": degrees,
                                                    "Model": model, "Params": params, "CP": CP}, ignore_index=True)
                counter += 1
            intervalID += 1

        DegreeVector.to_csv(Dir + '/DegreeVector_Interval.csv', encoding='utf-8', index=False, mode='w')
        DegreeVector.to_pickle(Dir + '/DegreeVector_Interval.pickle')
        return DegreeVector

    def SimulationConfusionMatrix(self,Dir, threshold, Mode='PLP-value', Plot=False):
        cpd = pd.read_csv(Dir + "/GraphComparison_Interval.csv", sep=',')
        GT = pd.read_pickle(Dir + "/DegreeVector_Interval.pickle")
        TP = TN = FP = FN = []
        cp = []
        gt = []
        y = []

        Ev = GT.CP.tolist()
        for i in range(len(Ev)):
            if Ev[i] < 0:
                y.append(1)
            else:
                y.append(0)
            if i < len(Ev) - 1 and Ev[i] != Ev[i + 1] and Ev[i] * Ev[i + 1] < 0:
                gt.append(i + 1)

        ks_cpd = cpd['KS-P-Value'].fillna(1).tolist()
        p_val_cpd = cpd['p-value'].fillna(1).tolist()
        if Mode == 'PLP-value':
            cpd = p_val_cpd
            for j in range(1, len(cpd)):
                if (cpd[j] >= threshold and cpd[j - 1] < threshold) or (cpd[j] < threshold and cpd[j - 1] >= threshold):
                    cp.append(j)
        else:
            cpd = ks_cpd
            for j in range(len(cpd)):
                if cpd[j] < threshold:
                    cp.append(j)

        TP = [p for p in cp if p in gt]
        FP = [p for p in cp if p not in gt]
        FN = [p for p in gt if p not in cp]
        TN = [p for p in Ev if p not in TP + FP + FN]
        if len(TP) + len(FN) > 0:
            Recall = len(TP) / float(len(TP) + len(FN))
        else:
            Recall = 0
        if len(TP) + len(FP) > 0:
            Precision = len(TP) / float(len(TP) + len(FP))
        else:
            Precision = 0
        if Plot:
            x = range(len(Ev))
            fig, axs = plt.subplots(nrows=1, ncols=1, sharex=True, dpi=80, figsize=(13, 12))
            axs.plot(x, y, 'b-')
            axs.plot(FN, [1.1] * len(FN), 'r*')
            axs.plot(TP, [1.05] * len(TP), 'g*')
            axs.plot(FP, [1.05] * len(FP), 'r*')
            axs.set(xlabel='Interval', ylabel='Norm')
            for i in range(len(TP)):
                axs.axvline(x=TP[i], color='g', linestyle='--')
            for i in range(len(FN)):
                axs.axvline(x=FN[i], color='r', linestyle='--')
            for i in range(len(FP)):
                axs.axvline(x=FP[i], color='r', linestyle='--')
        return len(TP), len(FP), len(TN), len(FN)

    def get_interval_vector(self,FullDegreeVector, IntervalStart, IntervalStop):
        WinDegreeV = FullDegreeVector.loc[(FullDegreeVector['Interval'] >= IntervalStart) &
                                          (FullDegreeVector['Interval'] <= IntervalStop)]
        WinDegree = []
        for dist in WinDegreeV.DegreeVector:
            if WinDegree == []:
                WinDegree = dist
            else:
                WinDegree = [x + y for x, y in zip(WinDegree, dist)]
        return WinDegree

    def create_directory(self,dir_name):
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

    def Get_Window_Vectors(self,FullDegreeVector, Window_size, slide, limit=0):
        WinDegreeVector = pd.DataFrame(columns=["Interval", "DegreeVector", "window"])
        IntervalStart = 1
        IntervalStop = IntervalStart + Window_size
        counter = 1
        while IntervalStop < FullDegreeVector.Interval.iloc[len(FullDegreeVector) - 1]:
            degrees = self.get_interval_vector(FullDegreeVector, IntervalStart, IntervalStop)
            WinDegreeVector = WinDegreeVector.append({"Interval": IntervalStart, "DegreeVector": degrees,
                                                      "window": [IntervalStart, IntervalStop]}, ignore_index=True)
            IntervalStart = IntervalStart + slide
            IntervalStop = IntervalStart + Window_size
            counter += 1
            if limit > 0 and counter > limit:
                break
        return WinDegreeVector

    def Create_SlidingWindow(self,FullDegreeVector, Window_size, slide, HomeDir):
        sub_directory = HomeDir + "/Interval" + str(Window_size) + "_" + str(slide)
        self.create_directory(sub_directory)
        WinDegreeVector = self.Get_Window_Vectors(FullDegreeVector, Window_size, slide, limit=0)
        WinDegreeVector.to_csv(sub_directory + '/DegreeVector_Interval.csv', encoding='utf-8', index=False, mode='w')
        WinDegreeVector.to_pickle(sub_directory + '/DegreeVector_Interval.pickle')
        return WinDegreeVector

    def get_EV(self,Dir):
        y = []
        gt = []
        GT = pd.read_pickle(Dir + "/DegreeVector_Interval.pickle")
        Ev = GT.CP.tolist()
        for i in range(len(Ev)):
            if Ev[i] < 0:
                y.append(1)
            else:
                y.append(0)
            if i < len(Ev) - 1 and Ev[i] != Ev[i + 1] and Ev[i] * Ev[i + 1] < 0:
                #gt.append(i + 1)
                gt.append(GT.Interval.iloc[i + 1])
        return gt, y

    def get_Change_Points(self,comparison, Mode,threshold,win_size):
        ks_cpd = comparison['KS-P-Value'].fillna(1).tolist()
        p_val_cpd = comparison['p-value'].fillna(1).tolist()
        cp = []
        if Mode == 'PLP-value':
            cpd = p_val_cpd
            for j in range(1, len(cpd)):
                if (cpd[j] >= threshold and cpd[j - 1] < threshold) or (cpd[j] < threshold and cpd[j - 1] >= threshold):
                    #cp.append((int(comparison.Interval.iloc[j]), (int(comparison.Interval.iloc[j + 1]) - 1)))
                    cp.append((int(comparison.Interval.iloc[j]), (int(comparison.Interval.iloc[j]) +win_size - 1)))
        else:
            cpd = ks_cpd
            for j in range(len(cpd)-1):
                if cpd[j] < threshold:
                    #cp.append((int(comparison.Interval.iloc[j]), (int(comparison.Interval.iloc[j + 1]) - 1)))
                    cp.append((int(comparison.Interval.iloc[j]), (int(comparison.Interval.iloc[j]) + win_size - 1)))
        return cp

    def get_windows(self,comparison):
        wins = []
        for j in range(len(comparison) - 1):
            wins.append((int(comparison.Interval.iloc[j]), (int(comparison.Interval.iloc[j + 1]) - 1)))
        return wins

    def Check_CM(self,comparison, Mode,threshold,base,win_size):
        TP = TN = FP = FN = []
        gt, y = self.get_EV(base)
        AllIntervals = self.get_Change_Points(comparison, Mode,0,win_size)
        cp = self.get_Change_Points(comparison, Mode,threshold,win_size)
        TP = TN = FP = FN = []
        total_distance = []
        Not_FN = []
        for index in range(len(gt) ):
            for icp in cp:
                if gt[index] >= icp[0] and gt[index] <= icp[1]:
                    if icp not in TP:
                        TP.append(icp)
                    Not_FN.append(gt[index])
                    total_distance.append(gt[index] - icp[0])
                    continue
                if gt[index] < icp[0]:
                    continue
        FN = [x for x in gt if x not in Not_FN]
        FP = [x for x in cp if x not in TP]
        #TN = [x for x in AllIntervals if x not in FN + FP + TP]
        TN = len(comparison) - len(FN) - len(FP) - len(TP)
        return len(TP), TN, len(FP), len(FN) , total_distance

    def Add_New_Copmarison(self,compDF,home_dir,win_dir,Window_size,slide,Mode,threshold):
        comparison = pd.read_csv(win_dir + "/GraphComparison_Interval.csv", sep=',')
        TP,TN,FP,FN,total_distance = self.Check_CM(comparison,Mode,threshold,home_dir,Window_size)
        if (float(TP) + FN) == 0:
            recall = 0
        else:
            recall = TP / (float(TP) + FN)
        if (float(TP) + FP) == 0:
            precision = 0
        else:
            precision = TP / (float(TP) + FP)
        if (float(TP)+TN+FP+FN) == 0:
            accuracy = 0
        else:
            accuracy = (TP+TN) / (float(TP)+TN+FP+FN)
        compDF = compDF.append({"Window": Window_size, "Slide" : slide,
                                                       "Mode":Mode,"threshold" : threshold,
                                                        "Accuracy": accuracy,
                                                        "Recall": recall,
                                                        "Precision": precision,
                                                        "TotalDistance": sum(total_distance),
                                                        "Distance":total_distance,
                                                        "DistanveAVG":sum(total_distance)/float(max(len(total_distance),1)),
                                                        "CM": [TP,TN,FP,FN],
                                                              }, ignore_index=True)
        return compDF

    def Operate(self,Activation):
        WindowsComparison = pd.DataFrame(
            columns=["Window", "Slide", "Mode", "threshold", 'Accuracy', 'Precision', 'Recall',
                     "Distance", "TotalDistance","DistanveAVG",'CM'])
        Dir = Activation['Directory']
        self.create_directory(Dir)
        logging.info("NumberOfVerrices: " + str(Activation['NetParams']['NumberOfVerrices']))
        logging.info("LifeTime: " + str(Activation['NetParams']['LifeTime']))
        logging.info("EventRate: " + str(Activation['NetParams']['EventRate']))
        logging.info("ContunityMEAN: " + str(Activation['NetParams']['ContunityMEAN']))
        logging.info("ContunitySTD: " + str(Activation['NetParams']['ContunitySTD']))
        logging.info("Tested_Window_Sizes: " + str(Activation['Tested_Window_Sizes']))
        logging.info("CmModes: " + str(Activation['CmModes']))
        Tested_Window_Sizes = Activation['Tested_Window_Sizes']
        Modes = Activation['CmModes']
        if Activation['Master']['Generate']:
            NumberOfVerrices = Activation['NetParams']['NumberOfVerrices']
            LifeTime = Activation['NetParams']['LifeTime']
            EventRate = Activation['NetParams']['EventRate']
            ContunityMEAN = Activation['NetParams']['ContunityMEAN']
            ContunitySTD = Activation['NetParams']['ContunitySTD']
            DegreeVector = self.GenerateRawData(NumberOfVerrices, LifeTime, EventRate, ContunityMEAN, ContunitySTD, Dir)
            logging.info("Master Generation Data Completed!")
        if Activation['Master']['Fit']:
            if 'Finite' in Activation['Master']:
                finite = Activation['Master']['Finite']
            else:
                finite = False
            if 'MinNodeToFit' in Activation['Master']:
                minnodetofit = Activation['Master']['MinNodeToFit']
            else:
                minnodetofit = 0
            fit1 = self.FitAll2(Mode='Synthetic', Weighted=True, DegreeType='ALL',Finite=finite,
                                PDFType="Cont", MinNodesToFit=minnodetofit, py_ver="JMM", OutputDir=Dir, Limit=0)
            logging.info("Master Fitting Completed")
        if Activation['Master']["Conf"]:
            self.LoadFitDataFromFile(Dir + "/GraphComparison_Interval.csv")
            self.LoadDegreeVectorFromFile(Dir + "/DegreeVector_Interval.pickle")
            conf1 = self.CalcConfidence(Reps=100, ConfidenceType=Activation['Master']["ConfType"], OutputDir=Dir)
            logging.info("Master Confidence Level Calculated")
        if Activation['Master']["ConfusionMatrix"]:
            for Mode, threshold in Modes:
                WindowsComparison = self.Add_New_Copmarison(WindowsComparison, Dir, Dir, 1, 1, Mode, threshold)
                WindowsComparison.to_csv(Dir + '/ConfusionMatrix' + '.csv', encoding='utf-8', index=False)
            logging.info("Master Confusion Matrix computed")
        if Activation['Slave']['Generate']:
            FullDegreeVector = self.LoadDegreeVectorFromFile(Dir + "/DegreeVector_Interval.pickle")
            for Window_size in Tested_Window_Sizes:
                for slide in range(1, Window_size + 1):
                    self.Create_SlidingWindow(FullDegreeVector, Window_size, slide, Dir)
            logging.info("Slaves Generation Data Completed!")
        if Activation['Slave']['Fit']:
            if 'Finite' in Activation['Slave']:
                finite = Activation['Slave']['Finite']
            else:
                finite = False
            if 'MinNodeToFit' in Activation['Slave']:
                minnodetofit = Activation['Slave']['MinNodeToFit']
            else:
                minnodetofit = 0
            for Window_size in Tested_Window_Sizes:
                for slide in range(1, Window_size + 1):
                    win_dir = Dir + "/Interval" + str(Window_size) + "_" + str(slide)
                    fit1 = self.FitAll2(Mode='Synthetic', Weighted=True, DegreeType='ALL', PDFType="Cont",
                                        Finite=finite, MinNodesToFit=minnodetofit, py_ver="JMM", OutputDir=win_dir, Limit=0)
                    logging.info(str(Window_size) + "/" + str(slide) + " is Completed!")
            logging.info("Slaves Fitting Completed")
        if Activation['Slave']['Compare']:
            for Window_size in Tested_Window_Sizes:
                for slide in range(1, Window_size + 1):
                    win_dir = Dir + "/Interval" + str(Window_size) + "_" + str(slide)
                    self.LoadFitDataFromFile(win_dir + "/GraphComparison_Interval.csv")
                    self.LoadDegreeVectorFromFile(win_dir + "/DegreeVector_Interval.pickle")
                    conf1 = self.CalcConfidence(Reps=100, ConfidenceType=Activation['Master']["ConfType"], OutputDir=win_dir, Limit=0)
                    logging.info(str(Window_size) + "/" + str(slide) + " is Completed!")
            logging.info("Slaves Comparing Completed")
        if Activation['Slave']["ConfusionMatrix"]:
            for Window_size in Tested_Window_Sizes:
                for slide in range(1, Window_size+1):
                    win_dir = Dir + "/Interval" + str(Window_size) + "_" + str(slide)
                    for Mode, threshold in Modes:
                        WindowsComparison = self.Add_New_Copmarison(WindowsComparison, Dir, win_dir,
                                                                    Window_size, slide, Mode, threshold)
            WindowsComparison.to_csv(Dir + '/ConfusionMatrix' + '.csv', encoding='utf-8', index=False)
            logging.info("Slaves Confusion Matrinx Completed")

