Creator "igraph version 0.7.1 Mon Jun 10 16:44:08 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "591"
    label "591"
  ]
  node
  [
    id 1
    name "605"
    label "605"
  ]
  node
  [
    id 2
    name "742"
    label "742"
  ]
  node
  [
    id 3
    name "215"
    label "215"
  ]
  node
  [
    id 4
    name "135"
    label "135"
  ]
  node
  [
    id 5
    name "602"
    label "602"
  ]
  node
  [
    id 6
    name "762"
    label "762"
  ]
  node
  [
    id 7
    name "761"
    label "761"
  ]
  node
  [
    id 8
    name "553"
    label "553"
  ]
  node
  [
    id 9
    name "393"
    label "393"
  ]
  node
  [
    id 10
    name "61"
    label "61"
  ]
  node
  [
    id 11
    name "769"
    label "769"
  ]
  node
  [
    id 12
    name "396"
    label "396"
  ]
  node
  [
    id 13
    name "66"
    label "66"
  ]
  node
  [
    id 14
    name "416"
    label "416"
  ]
  node
  [
    id 15
    name "541"
    label "541"
  ]
  node
  [
    id 16
    name "883"
    label "883"
  ]
  node
  [
    id 17
    name "790"
    label "790"
  ]
  node
  [
    id 18
    name "45"
    label "45"
  ]
  node
  [
    id 19
    name "29"
    label "29"
  ]
  node
  [
    id 20
    name "657"
    label "657"
  ]
  node
  [
    id 21
    name "180"
    label "180"
  ]
  node
  [
    id 22
    name "534"
    label "534"
  ]
  node
  [
    id 23
    name "5"
    label "5"
  ]
  node
  [
    id 24
    name "324"
    label "324"
  ]
  node
  [
    id 25
    name "912"
    label "912"
  ]
  node
  [
    id 26
    name "308"
    label "308"
  ]
  node
  [
    id 27
    name "771"
    label "771"
  ]
  node
  [
    id 28
    name "638"
    label "638"
  ]
  node
  [
    id 29
    name "616"
    label "616"
  ]
  node
  [
    id 30
    name "670"
    label "670"
  ]
  node
  [
    id 31
    name "450"
    label "450"
  ]
  node
  [
    id 32
    name "673"
    label "673"
  ]
  node
  [
    id 33
    name "77"
    label "77"
  ]
  node
  [
    id 34
    name "267"
    label "267"
  ]
  node
  [
    id 35
    name "260"
    label "260"
  ]
  node
  [
    id 36
    name "168"
    label "168"
  ]
  node
  [
    id 37
    name "984"
    label "984"
  ]
  node
  [
    id 38
    name "227"
    label "227"
  ]
  node
  [
    id 39
    name "2"
    label "2"
  ]
  node
  [
    id 40
    name "586"
    label "586"
  ]
  node
  [
    id 41
    name "105"
    label "105"
  ]
  node
  [
    id 42
    name "695"
    label "695"
  ]
  node
  [
    id 43
    name "743"
    label "743"
  ]
  node
  [
    id 44
    name "789"
    label "789"
  ]
  node
  [
    id 45
    name "356"
    label "356"
  ]
  node
  [
    id 46
    name "643"
    label "643"
  ]
  node
  [
    id 47
    name "808"
    label "808"
  ]
  node
  [
    id 48
    name "685"
    label "685"
  ]
  node
  [
    id 49
    name "684"
    label "684"
  ]
  node
  [
    id 50
    name "435"
    label "435"
  ]
  node
  [
    id 51
    name "292"
    label "292"
  ]
  edge
  [
    source 17
    target 36
    weight 1
  ]
  edge
  [
    source 10
    target 31
    weight 1
  ]
  edge
  [
    source 23
    target 30
    weight 1
  ]
  edge
  [
    source 21
    target 3
    weight 3
  ]
  edge
  [
    source 36
    target 13
    weight 3
  ]
  edge
  [
    source 40
    target 34
    weight 1
  ]
  edge
  [
    source 46
    target 48
    weight 1
  ]
  edge
  [
    source 45
    target 40
    weight 1
  ]
  edge
  [
    source 36
    target 6
    weight 1
  ]
  edge
  [
    source 1
    target 44
    weight 1
  ]
  edge
  [
    source 40
    target 45
    weight 2
  ]
  edge
  [
    source 20
    target 15
    weight 2
  ]
  edge
  [
    source 45
    target 36
    weight 1
  ]
  edge
  [
    source 19
    target 47
    weight 1
  ]
  edge
  [
    source 27
    target 2
    weight 1
  ]
  edge
  [
    source 49
    target 4
    weight 1
  ]
  edge
  [
    source 13
    target 38
    weight 1
  ]
  edge
  [
    source 26
    target 32
    weight 1
  ]
  edge
  [
    source 36
    target 25
    weight 6
  ]
  edge
  [
    source 40
    target 14
    weight 1
  ]
  edge
  [
    source 51
    target 11
    weight 1
  ]
  edge
  [
    source 41
    target 29
    weight 1
  ]
  edge
  [
    source 36
    target 45
    weight 1
  ]
  edge
  [
    source 2
    target 27
    weight 1
  ]
  edge
  [
    source 40
    target 37
    weight 1
  ]
  edge
  [
    source 36
    target 18
    weight 3
  ]
  edge
  [
    source 45
    target 25
    weight 1
  ]
  edge
  [
    source 24
    target 22
    weight 1
  ]
  edge
  [
    source 9
    target 16
    weight 1
  ]
  edge
  [
    source 24
    target 28
    weight 1
  ]
  edge
  [
    source 36
    target 35
    weight 1
  ]
  edge
  [
    source 12
    target 7
    weight 1
  ]
  edge
  [
    source 44
    target 1
    weight 1
  ]
  edge
  [
    source 13
    target 36
    weight 3
  ]
  edge
  [
    source 40
    target 50
    weight 1
  ]
  edge
  [
    source 45
    target 5
    weight 1
  ]
  edge
  [
    source 24
    target 0
    weight 1
  ]
  edge
  [
    source 17
    target 39
    weight 4
  ]
  edge
  [
    source 36
    target 17
    weight 1
  ]
  edge
  [
    source 45
    target 43
    weight 1
  ]
  edge
  [
    source 31
    target 42
    weight 1
  ]
  edge
  [
    source 33
    target 36
    weight 1
  ]
  edge
  [
    source 13
    target 18
    weight 2
  ]
  edge
  [
    source 47
    target 19
    weight 1
  ]
  edge
  [
    source 23
    target 8
    weight 1
  ]
  edge
  [
    source 36
    target 33
    weight 2
  ]
]
