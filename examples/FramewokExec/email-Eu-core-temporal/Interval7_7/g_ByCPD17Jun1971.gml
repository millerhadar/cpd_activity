Creator "igraph version 0.7.1 Mon Jun 10 16:44:32 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "586"
    label "586"
  ]
  node
  [
    id 1
    name "521"
    label "521"
  ]
  node
  [
    id 2
    name "782"
    label "782"
  ]
  node
  [
    id 3
    name "744"
    label "744"
  ]
  node
  [
    id 4
    name "126"
    label "126"
  ]
  node
  [
    id 5
    name "885"
    label "885"
  ]
  node
  [
    id 6
    name "790"
    label "790"
  ]
  node
  [
    id 7
    name "946"
    label "946"
  ]
  node
  [
    id 8
    name "862"
    label "862"
  ]
  node
  [
    id 9
    name "575"
    label "575"
  ]
  node
  [
    id 10
    name "472"
    label "472"
  ]
  node
  [
    id 11
    name "643"
    label "643"
  ]
  node
  [
    id 12
    name "260"
    label "260"
  ]
  node
  [
    id 13
    name "168"
    label "168"
  ]
  node
  [
    id 14
    name "984"
    label "984"
  ]
  node
  [
    id 15
    name "324"
    label "324"
  ]
  node
  [
    id 16
    name "178"
    label "178"
  ]
  node
  [
    id 17
    name "685"
    label "685"
  ]
  node
  [
    id 18
    name "367"
    label "367"
  ]
  node
  [
    id 19
    name "425"
    label "425"
  ]
  node
  [
    id 20
    name "912"
    label "912"
  ]
  edge
  [
    source 20
    target 19
    weight 1
  ]
  edge
  [
    source 15
    target 18
    weight 1
  ]
  edge
  [
    source 17
    target 11
    weight 2
  ]
  edge
  [
    source 13
    target 1
    weight 2
  ]
  edge
  [
    source 16
    target 8
    weight 1
  ]
  edge
  [
    source 13
    target 20
    weight 3
  ]
  edge
  [
    source 2
    target 4
    weight 1
  ]
  edge
  [
    source 20
    target 10
    weight 1
  ]
  edge
  [
    source 13
    target 0
    weight 1
  ]
  edge
  [
    source 13
    target 14
    weight 1
  ]
  edge
  [
    source 13
    target 5
    weight 1
  ]
  edge
  [
    source 13
    target 12
    weight 3
  ]
  edge
  [
    source 13
    target 7
    weight 1
  ]
  edge
  [
    source 6
    target 4
    weight 2
  ]
  edge
  [
    source 13
    target 3
    weight 1
  ]
  edge
  [
    source 12
    target 13
    weight 1
  ]
  edge
  [
    source 13
    target 9
    weight 1
  ]
  edge
  [
    source 13
    target 10
    weight 2
  ]
]
