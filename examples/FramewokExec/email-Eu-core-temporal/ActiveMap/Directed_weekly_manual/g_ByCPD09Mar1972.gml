Creator "igraph version 0.7.1 Mon Jun 10 16:44:34 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "66"
    label "66"
  ]
  node
  [
    id 1
    name "761"
    label "761"
  ]
  node
  [
    id 2
    name "575"
    label "575"
  ]
  node
  [
    id 3
    name "62"
    label "62"
  ]
  node
  [
    id 4
    name "135"
    label "135"
  ]
  node
  [
    id 5
    name "232"
    label "232"
  ]
  node
  [
    id 6
    name "416"
    label "416"
  ]
  node
  [
    id 7
    name "323"
    label "323"
  ]
  node
  [
    id 8
    name "369"
    label "369"
  ]
  node
  [
    id 9
    name "912"
    label "912"
  ]
  node
  [
    id 10
    name "305"
    label "305"
  ]
  node
  [
    id 11
    name "77"
    label "77"
  ]
  node
  [
    id 12
    name "260"
    label "260"
  ]
  node
  [
    id 13
    name "168"
    label "168"
  ]
  node
  [
    id 14
    name "266"
    label "266"
  ]
  node
  [
    id 15
    name "906"
    label "906"
  ]
  node
  [
    id 16
    name "949"
    label "949"
  ]
  node
  [
    id 17
    name "472"
    label "472"
  ]
  node
  [
    id 18
    name "356"
    label "356"
  ]
  node
  [
    id 19
    name "535"
    label "535"
  ]
  node
  [
    id 20
    name "532"
    label "532"
  ]
  node
  [
    id 21
    name "439"
    label "439"
  ]
  node
  [
    id 22
    name "648"
    label "648"
  ]
  node
  [
    id 23
    name "800"
    label "800"
  ]
  node
  [
    id 24
    name "292"
    label "292"
  ]
  node
  [
    id 25
    name "804"
    label "804"
  ]
  edge
  [
    source 1
    target 25
    weight 1
  ]
  edge
  [
    source 9
    target 2
    weight 1
  ]
  edge
  [
    source 10
    target 5
    weight 1
  ]
  edge
  [
    source 9
    target 21
    weight 1
  ]
  edge
  [
    source 4
    target 24
    weight 1
  ]
  edge
  [
    source 25
    target 20
    weight 1
  ]
  edge
  [
    source 21
    target 16
    weight 1
  ]
  edge
  [
    source 19
    target 8
    weight 1
  ]
  edge
  [
    source 25
    target 1
    weight 2
  ]
  edge
  [
    source 9
    target 6
    weight 1
  ]
  edge
  [
    source 0
    target 2
    weight 1
  ]
  edge
  [
    source 9
    target 22
    weight 1
  ]
  edge
  [
    source 9
    target 5
    weight 1
  ]
  edge
  [
    source 9
    target 14
    weight 1
  ]
  edge
  [
    source 23
    target 3
    weight 1
  ]
  edge
  [
    source 9
    target 11
    weight 1
  ]
  edge
  [
    source 9
    target 18
    weight 1
  ]
  edge
  [
    source 9
    target 13
    weight 2
  ]
  edge
  [
    source 7
    target 15
    weight 1
  ]
  edge
  [
    source 9
    target 16
    weight 1
  ]
  edge
  [
    source 9
    target 12
    weight 1
  ]
  edge
  [
    source 9
    target 17
    weight 2
  ]
  edge
  [
    source 24
    target 4
    weight 1
  ]
]
